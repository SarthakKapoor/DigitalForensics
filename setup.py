from setuptools import setup

setup(

    name='DigitalForensics',

    url='https://gitlab.com/SarthakKapoor/DigitalForensics',

    author="Sarthak Kapoor",

    author_email='2349677k@student.gla.ac.uk',

    packages=['forensics_modules', 'forensics_hashing', 'forensics_search_indexing', 'forensics_image_meta_data_search', 'forensics_time',
              'features', 'forensics_network_investigation', 'multiprocessing'],

    install_requires={'behave', 'Pillow', 'ntplib', 'nltk', 'ExifRead', 'compare', 'wxPython'},

    version='0.1',

    license='MIT',

    description='An example of a python package from pre-existing code',

)
