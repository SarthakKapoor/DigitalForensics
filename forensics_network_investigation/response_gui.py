import wx
import sys
from forensics_network_investigation import response_ping
import socket

from time import gmtime, strftime


# Function to find which port is active
def scan_ping(event):
    if host_end.GetValue() < host_start.GetValue():
        dlg = wx.MessageDialog(mainWin, "Invalid Local Host Selection", "Confirm", wx.OK | wx.ICON_EXCLAMATION)
        result = dlg.ShowModal()
        dlg.Destroy()
        return

    mainWin.StatusBar.SetStatusText('Execution of Port Sweep started .... Please Wait')

    utc_start = gmtime()
    utc = strftime("%a, %d %b %Y %X +0000", utc_start)
    results.AppendText("\n\nPing Sweep Started: " + utc + "\n\n")

    base_IP = str(ipaRange.GetValue()) + '.' + str(ipbRange.GetValue()) + '.' + str(ipcRange.GetValue()) + '.'

    ip_range = []

    for i in range(host_start.GetValue(), (host_end.GetValue() + 1)):
        ip_range.append(base_IP + str(i))

    for ip_address in ip_range:

        try:

            mainWin.StatusBar.SetStatusText('Pinging IP: ' + ip_address)
            timeout = 2
            packet_size = 64

            delay = response_ping.do_one(ip_address, timeout,  packet_size)

            results.AppendText(ip_address + '\t')

            if delay is not None:

                results.AppendText('   Response Success')
                results.AppendText('   Response Time: ' + str(delay) + ' Seconds')
                results.AppendText("\n")
            else:

                results.AppendText('   Response Timeout')
                results.AppendText("\n")

        except socket.error as e:

            results.AppendText(ip_address)
            results.AppendText('   Response Failed: ')
            results.AppendText('e.message')
            results.AppendText("\n")

    utc_end = gmtime()
    utc = strftime("%a, %d %b %Y %X +0000", utc_end)
    results.AppendText("\nPing Sweep Ended: " + utc + "\n\n")

    mainWin.StatusBar.SetStatusText('')


def end_program(event):
    sys.exit()


app = wx.App()

mainWin = wx.Frame(None, title="Simple Ping (ICMP) Sweeper 1.0", size=(1000, 600))

panelAction = wx.Panel(mainWin)

scanButton = wx.Button(panelAction, label='Scan')
scanButton.Bind(wx.EVT_BUTTON, scan_ping)

exitButton = wx.Button(panelAction, label='Exit')
exitButton.Bind(wx.EVT_BUTTON, end_program)

results = wx.TextCtrl(panelAction, style=wx.TE_MULTILINE | wx.HSCROLL)

ipaRange = wx.SpinCtrl(panelAction, -1, '')
ipaRange.SetRange(0, 255)
ipaRange.SetValue(127)

ipbRange = wx.SpinCtrl(panelAction, -1, '')
ipbRange.SetRange(0, 255)
ipbRange.SetValue(0)

ipcRange = wx.SpinCtrl(panelAction, -1, '')
ipcRange.SetRange(0, 255)
ipcRange.SetValue(0)

ipLabel = wx.StaticText(panelAction, label="IP Base: ")

host_start = wx.SpinCtrl(panelAction, -1, '')
host_start.SetRange(0, 255)
host_start.SetValue(1)

host_end = wx.SpinCtrl(panelAction, -1, '')
host_end.SetRange(0, 255)
host_end.SetValue(10)

HostStartLabel = wx.StaticText(panelAction, label="Host Start: ")
HostEndLabel = wx.StaticText(panelAction, label="Host End: ")

actionBox = wx.BoxSizer()
actionBox.Add(scanButton, proportion=1, flag=wx.LEFT, border=5)
actionBox.Add(exitButton, proportion=0, flag=wx.LEFT, border=5)

actionBox.Add(ipLabel, proportion=0, flag=wx.LEFT | wx.CENTER, border=5)

actionBox.Add(ipaRange, proportion=0, flag=wx.LEFT, border=5)
actionBox.Add(ipbRange, proportion=0, flag=wx.LEFT, border=5)
actionBox.Add(ipcRange, proportion=0, flag=wx.LEFT, border=5)

actionBox.Add(HostStartLabel, proportion=0, flag=wx.LEFT | wx.CENTER, border=5)
actionBox.Add(host_start, proportion=0, flag=wx.LEFT, border=5)

actionBox.Add(HostEndLabel, proportion=0, flag=wx.LEFT | wx.CENTER, border=5)
actionBox.Add(host_end, proportion=0, flag=wx.LEFT, border=5)

vertBox = wx.BoxSizer(wx.VERTICAL)
vertBox.Add(actionBox, proportion=0, flag=wx.EXPAND | wx.ALL, border=5)
vertBox.Add(results, proportion=1, flag=wx.EXPAND | wx.LEFT | wx.BOTTOM | wx.RIGHT, border=5)

mainWin.CreateStatusBar()

panelAction.SetSizer(vertBox)

mainWin.Show()

app.MainLoop()
