import os
import select
import socket
import struct
import time

icmp_echo_request = 8


# Function for conducting a checksum to identify the changed ip
def checksum(source_string):
    sum = 0
    count_to = (len(source_string) / 2) * 2
    count = 0

    while count < count_to:
        this_val = ord(source_string[count + 1]) * 256 + ord(source_string[count])
        sum = sum + this_val
        sum = sum & 0xffffffff
        count = count + 2
    if count_to < len(source_string):
        sum = sum + ord(source_string[len(source_string) - 1])
        sum = sum & 0xffffffff
    sum = (sum >> 16) + (sum & 0xffff)
    sum = sum + (sum >> 16)
    answer = ~sum
    answer = answer & 0xffff

    answer = answer >> 8 | (answer << 8 & 0xff00)

    return answer


# Function for receiving
def receive_one_ping(my_socket, ID, timeout):
    timeLeft = timeout
    while True:
        started_select = time.time()
        what_ready = select.select([my_socket], [], [], timeLeft)
        howLongInSelect = (time.time() - started_select)
        if not what_ready[0]:  # Timeout
            return

        receive_time = time.time()
        rec_packet, addr = my_socket.recvfrom(1024)
        icmp_header = rec_packet[20:28]
        type, code, checksum, packet_ID, sequence = struct.unpack(
            "bbHHh", icmp_header
        )
        if packet_ID == ID:
            bytes_in_double = struct.calcsize("d")
            time_sent = struct.unpack("d", rec_packet[28:28 + bytes_in_double])[0]
            return receive_time - time_sent
        timeLeft = timeLeft - howLongInSelect
        if timeLeft <= 0:
            return


# Function for sending
def send_one_ping(my_socket, dest_addr, ID):
    dest_addr = socket.gethostbyname(dest_addr)
    my_checksum = 0
    header = struct.pack("bbHHh", icmp_echo_request, 0, my_checksum, ID, 1)
    bytesInDouble = struct.calcsize("d")
    data = (192 - bytesInDouble) * "Q"
    data = struct.pack("d", time.time()) + data.encode()
    my_checksum = checksum(header + data)

    header = struct.pack("bbHHh", icmp_echo_request, 0, socket.htons(my_checksum), ID, 1)
    packet = header + data
    my_socket.sendto(packet, (dest_addr, 1))  # Don't know about the 1


# Time out check
def do_one(dest_addr, timeout):
    icmp = socket.getprotobyname("icmp")
    try:
        my_socket = socket.socket(socket.AF_INET, socket.SOCK_RAW,
                                  icmp)
    except socket.error as e:
        errnumber, msg = e.args
        if errnumber == 1:
            msg = msg + "processor as a root can only send the IMCP messages"
            raise socket.error(msg)
        raise

    my_ID = os.getpid() & 0xFFFF
    send_one_ping(my_socket, dest_addr, my_ID)
    delay = receive_one_ping(my_socket, my_ID, timeout)
    my_socket.close()
    return delay


# Verbose check
def verbose_ping(dest_addr, timeout=2, count=4):
    for i in range(count):
        print("ping %s. . ." % dest_addr),
        try:
            delay = do_one(dest_addr, timeout)
        except socket.gaierror as e:
            print("(failed.(socket error:'%s')" % e[1])
            break
        if delay is None:
            print("failed.(timeout within %ssec.)" % timeout)
        else:
            delay = delay * 1000
        print("get ping in %0.4fms" % delay)
        print()


if __name__ == '__main__':
    verbose_ping("heise.de")
    verbose_ping("google.com")
    verbose_ping("a-test-url-taht-is-not-available.com")
    verbose_ping("192.168.1.1")
