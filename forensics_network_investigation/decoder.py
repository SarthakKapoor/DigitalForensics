import socket
from struct import *

tcp = 6
udp = 17


# Function to search all the packets and display the packets
def packet_extractor(packet, switch_display):
    packet_strip = packet[0:20]

    header_tuple_ip = unpack('!BBHHHBBH4s4s', packet_strip)

    ver_len = header_tuple_ip[0]
    dscp_ecn = header_tuple_ip[1]
    length_packet = header_tuple_ip[2]
    ID_packet = header_tuple_ip[3]
    frag_flag = header_tuple_ip[4]
    living_time = header_tuple_ip[5]
    protocol = header_tuple_ip[6]
    check_sum = header_tuple_ip[7]
    source_ip = header_tuple_ip[8]
    dest_ip = header_tuple_ip[9]

    version = ver_len >> 4  # Max limit is the version Number
    length = ver_len & 0x0F  # Min limit represents the size
    ip_hdr_length = length * 4  # Header length calculation in bytes

    source_address = socket.inet_ntoa(source_ip)
    destination_address = socket.inet_ntoa(dest_ip)

    if switch_display:
        print('IP Header')
        print('-----------------------')
        print('Version:         ' + str(version))
        print('Packet Length:   ' + str(length_packet) + ' bytes')
        print('Header Length:   ' + str(ip_hdr_length) + ' bytes')
        print('TTL:             ' + str(living_time))
        print('Protocol:        ' + str(protocol))
        print('Checksum:        ' + hex(check_sum))
        print('Source IP:       ' + str(source_address))
        print('Destination IP:  ' + str(destination_address))

    if protocol == tcp:

        strip_tcp_header = packet[ip_hdr_length:ip_hdr_length + 20]

        tcp_header_buffer = unpack('!HHLLBBHHH', strip_tcp_header)

        source_port = tcp_header_buffer[0]
        destination_port = tcp_header_buffer[1]
        sequence_number = tcp_header_buffer[2]
        acknowledgement = tcp_header_buffer[3]
        data_offset_reserve = tcp_header_buffer[4]
        tcp_header_length = (data_offset_reserve >> 4) * 4
        tcp_check_sum = tcp_header_buffer[7]

        if switch_display:
            print()
            print('TCP Header')
            print('-------------------')
            print('Source Port:       ' + str(source_port))
            print('Destination Port : ' + str(destination_port))
            print('Sequence Number :  ' + str(sequence_number))
            print('Acknowledgement :  ' + str(acknowledgement))
            print('TCP Header Length: ' + str(tcp_header_length) + ' bytes')
            print('TCP Checksum:      ' + hex(tcp_check_sum))
            print()

        return ['TCP', source_address, source_port, destination_address, destination_port]

    elif protocol == udp:

        strip_udp_header = packet[ip_hdr_length:ip_hdr_length + 8]

        udp_header_buffer = unpack('!HHHH', strip_udp_header)
        source_port = udp_header_buffer[0]
        destination_port = udp_header_buffer[1]
        udp_length = udp_header_buffer[2]
        udp_checksum = udp_header_buffer[3]

        if switch_display:
            print()
            print('UDP Header')
            print('-------------------')
            print('Source Port:       ' + str(source_port))
            print('Destination Port : ' + str(destination_port))
            print('UDP Length:        ' + str(udp_length) + ' bytes')
            print('UDP Checksum:      ' + hex(udp_checksum))
            print()

        return ['UDP', source_address, source_port, destination_address, destination_port]

    else:

        if switch_display:
            print('Found Protocol : ' + str(protocol))

        return ['Unsupported', source_address, 0, destination_address, 0]
