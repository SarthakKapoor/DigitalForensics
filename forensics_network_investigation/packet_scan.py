import os
import sys
import socket
import signal

from forensics_network_investigation import decoder
from forensics_network_investigation import command_parser
from forensics_network_investigation import csv_read_write
from forensics_network_investigation import logging_class

userArgs = command_parser.commandline()

logPath = os.path.join(userArgs.outPath, "ForensicLog.txt")
oLog = logging_class.ForensicLog(logPath)

oLog.log_write("INFO", "Packet scan initiated")

csvPath = os.path.join(userArgs.outPath, "ps_results.csv")
oCSV = csv_read_write._CSVWriter(csvPath)

if userArgs.TCP:
    PROTOCOL = socket.IPPROTO_TCP
elif userArgs.UDP:
    PROTOCOL = socket.IPPROTO_UDP
else:
    print('Capture protocol not selected')
    sys.exit()

if userArgs.verbose:
    VERBOSE = True
else:
    VERBOSE = False

capture_duration = userArgs.minutes * 60


class TimeOut(Exception):
    pass


# Function for controlling the search by checking the sockets with error handling and time out
def handler(signum, frame):
    print('timeout received', signum)
    raise TimeOut()


ter = os.system("ifconfig eth0 promisc")

if ter == 0:

    oLog.log_write("INFO", 'Promiscious Mode Enabled')  # To check if the mode is enabled

    try:
        the_socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, PROTOCOL)
        oLog.log_write("INFO", 'Raw Socket Open')
    except:

        oLog.log_write("ERROR", 'Raw Socket Open Failed')
        del oLog
        if VERBOSE:
            print('Error Opening Raw Socket')
        sys.exit()

    signal.signal(signal.SIGALRM, handler)
    signal.alarm(capture_duration)

    ip_observations = []  # Dictionary to store the fetched ip addresses

    try:

        while True:
            recv_buffer, addr = the_socket.recvfrom(255)

            content = decoder.packet_extractor(recv_buffer, VERBOSE)

            ip_observations.append(content)

            oLog.log_write('INFO',
                           '  RECV: ' + content[0] +
                           '  SRC : ' + content[1] +
                           '  DST : ' + content[3])

    except TimeOut:
        pass

    ret = os.system("ifconfig eth0 -promisc")
    oLog.log_write("INFO", 'Promiscious Mode Disabled')

    the_socket.close()
    oLog.log_write("INFO", 'Raw Socket Closed')

    unique_src = set(map(tuple, ip_observations))
    final_list = list(unique_src)
    final_list.sort()

    for packet in final_list:
        oCSV.write_csv_row(packet)

    oLog.log_write('INFO', 'Program End')

    del oLog
    del oCSV

else:
    print('Promiscious Mode not Set')

if __name__ == '__main__':
    csv_read_write_VERSION = '1.0'
