import socket

server_socket = socket.socket()    # For opening a socket

local_host = socket.gethostname()  # For fetching the address of local host

local_port = 5555  # To accept connections through any local port

server_socket.bind((local_host, local_port))   # For binding local host with the specified port

server_socket.listen(1)  # For starting to listen to the connections

print('Connection Request.... Waiting')
conn, client_info = server_socket.accept()   # To wait for connection to be established

print('Connection Received From:', client_info)  # Display message once the connection is established

conn.send('Connection Confirmed:' + 'IP:' + client_info[0] + 'Port:' + str(client_info[1]))
# To send message using connection object to the connector including the port used and the IP Address in response



