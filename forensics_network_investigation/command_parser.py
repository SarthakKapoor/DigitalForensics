import argparse
import os


# For passing arguments through the command line while program execution
def commandline():
    parser = argparse.ArgumentParser('Packet search initiated')

    parser.add_argument('-v', '--verbose', help="Display packet details", action='store_true')

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--TCP', help='TCP Packet Capture', action='store_true')
    group.add_argument('--UDP', help='UDP Packet Capture', action='store_true')

    parser.add_argument('-m', '--minutes', help='Capture Duration in minutes', type=int)
    parser.add_argument('-p', '--outPath', type=validate_directory, required=True, help="Output Directory")

    the_args = parser.parse_args()

    return the_args


# Test case
# Check if the write operation works for the provided path
def validate_directory(the_dir):
    if not os.path.isdir(the_dir):
        raise argparse.ArgumentTypeError('Directory does not exist')

    if os.access(the_dir, os.W_OK):
        return the_dir
    else:
        raise argparse.ArgumentTypeError('Directory is not writable')
