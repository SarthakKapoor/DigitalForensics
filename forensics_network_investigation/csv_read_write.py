import csv
import logging

log = logging.getLogger('main.psnmt')


# Class and nested function to perform the write operation in the CSV file for the result fields.
class CSVWriter:

    def __init__(self, file_name):
        try:
            self.csvFile = open(file_name, 'wb')
            self.writer = csv.writer(self.csvFile, delimiter=',', quoting=csv.QUOTE_ALL)
            self.writer.writerow(('Protocol', 'Source IP', 'Source Port', 'Destination IP', 'Destination Port'))
        except:
            log.error('CSV File Error')

    def write_csv_row(self, row):

        self.writer.writerow((row[0], row[1], str(row[2]), row[3], str(row[4])))

    def __del__(self):
        self.csvFile.close()
