import wx
import sys
from forensics_network_investigation import response_ping
from socket import *

from time import gmtime, strftime


# Function to traverse through ports
def scan_port(event):
    if portEnd.GetValue() < portStart.GetValue():
        dlg = wx.MessageDialog(mainWin, "Invalid Host Port Selection", "Confirm", wx.OK | wx.ICON_EXCLAMATION)
        result = dlg.ShowModal()
        dlg.Destroy()
        return

    mainWin.StatusBar.SetStatusText('Executing Port Scan .... Please Wait')

    utc_start = gmtime()
    utc = strftime("%a, %d %b %Y %X +0000", utc_start)
    results.AppendText("\n\nPort Scan Started: " + utc + "\n\n")

    base_IP = str(ipaRange.GetValue()) + '.' + str(ipbRange.GetValue()) + '.' + str(ipcRange.GetValue()) + '.' + str(
        ipdRange.GetValue())

    for port in range(portStart.GetValue(), portEnd.GetValue() + 1):

        try:

            mainWin.StatusBar.SetStatusText('Scanning: ' + base_IP + ' Port: ' + str(port))

            req_socket = socket(AF_INET, SOCK_STREAM)

            response = req_socket.connect_ex((base_IP, port))

            if response == 0:

                results.AppendText(base_IP + '\t' + str(port) + '\t')
                results.AppendText('Open')
                results.AppendText("\n")
            else:
                if displayAll.GetValue():
                    results.AppendText(base_IP + '\t' + str(port) + '\t')
                    results.AppendText('Closed')
                    results.AppendText("\n")

            req_socket.close()

        except socket.error as e:

            results.AppendText(base_IP + '\t' + str(port) + '\t')
            results.AppendText('Failed: ')
            results.AppendText(e.message)
            results.AppendText("\n")

    utc_end = gmtime()
    utc = strftime("%a, %d %b %Y %X +0000", utc_end)
    results.AppendText("\nPort Scan Ended: " + utc + "\n\n")

    mainWin.StatusBar.SetStatusText('')


def end_program(event):
    sys.exit()


# For GUI
app = wx.App()

mainWin = wx.Frame(None, title="Simple Port Scanner", size=(1200, 600))

panelAction = wx.Panel(mainWin)

displayAll = wx.CheckBox(panelAction, -1, 'Display All', (10, 10))
displayAll.SetValue(True)

scanButton = wx.Button(panelAction, label='Scan')
scanButton.Bind(wx.EVT_BUTTON, scan_port)

exitButton = wx.Button(panelAction, label='Exit')
exitButton.Bind(wx.EVT_BUTTON, end_program)

results = wx.TextCtrl(panelAction, style=wx.TE_MULTILINE | wx.HSCROLL)

ipaRange = wx.SpinCtrl(panelAction, -1, '')
ipaRange.SetRange(0, 255)
ipaRange.SetValue(127)

ipbRange = wx.SpinCtrl(panelAction, -1, '')
ipbRange.SetRange(0, 255)
ipbRange.SetValue(0)

ipcRange = wx.SpinCtrl(panelAction, -1, '')
ipcRange.SetRange(0, 255)
ipcRange.SetValue(0)

ipdRange = wx.SpinCtrl(panelAction, -1, '')
ipdRange.SetRange(0, 255)
ipdRange.SetValue(1)

ipLabel = wx.StaticText(panelAction, label="IP Address: ")

portStart = wx.SpinCtrl(panelAction, -1, '')
portStart.SetRange(1, 1025)
portStart.SetValue(1)

portEnd = wx.SpinCtrl(panelAction, -1, '')
portEnd.SetRange(1, 1025)
portEnd.SetValue(5)

PortStartLabel = wx.StaticText(panelAction, label="Port Start: ")
PortEndLabel = wx.StaticText(panelAction, label="Port  End: ")


action_box = wx.BoxSizer()

action_box.Add(displayAll, proportion=0, flag=wx.LEFT | wx.CENTER, border=5)
action_box.Add(scanButton, proportion=0, flag=wx.LEFT, border=5)
action_box.Add(exitButton, proportion=0, flag=wx.LEFT, border=5)

action_box.Add(ipLabel, proportion=0, flag=wx.LEFT | wx.CENTER, border=5)

action_box.Add(ipaRange, proportion=0, flag=wx.LEFT, border=5)
action_box.Add(ipbRange, proportion=0, flag=wx.LEFT, border=5)
action_box.Add(ipcRange, proportion=0, flag=wx.LEFT, border=5)
action_box.Add(ipdRange, proportion=0, flag=wx.LEFT, border=5)

action_box.Add(PortStartLabel, proportion=0, flag=wx.LEFT | wx.CENTER, border=5)
action_box.Add(portStart, proportion=0, flag=wx.LEFT, border=5)

action_box.Add(PortEndLabel, proportion=0, flag=wx.LEFT | wx.CENTER, border=5)
action_box.Add(portEnd, proportion=0, flag=wx.LEFT, border=5)

vert_box = wx.BoxSizer(wx.VERTICAL)
vert_box.Add(action_box, proportion=0, flag=wx.EXPAND | wx.ALL, border=5)
vert_box.Add(results, proportion=1, flag=wx.EXPAND | wx.LEFT | wx.BOTTOM | wx.RIGHT, border=5)

mainWin.CreateStatusBar()

panelAction.SetSizer(vert_box)

mainWin.Show()

app.MainLoop()
