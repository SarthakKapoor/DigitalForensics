import socket

MAX_BUFFER = 1024  # Receiver Size

client_socket = socket.socket()    # For opening a socket

local_host = socket.gethostname()  # For fetching the address of the local host

local_port = 5555   # For establishing connections through any local port

client_socket.connect((local_host, local_port))  # To establish a connection between the local port and the local host

msg = client_socket.recv(MAX_BUFFER)  # To wait for the connection to be established and for the response to be received

print(msg)   # To print the received message

client_socket.close()  # To close the connection
