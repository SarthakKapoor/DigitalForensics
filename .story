Forensics Hashing:

1.
   Given a set of files or a data set in a directory
   when a type of hashing is performed
   then a unique one way hash values should be created for each file
   and should match with the re-calculated values of the same set of files or data set

   Acceptance Test:
   The hash values for both or multiple instances should match with each other to prove preservation of
   evidence

2.
   Given a set of already known evidence files in a single or multiple directories for investigation
   and a corresponding hash value for each file is present
   when the hash values are compared
   then any suspicious directory on a system could be searched for with the same set of values and the
   matched values could be reported as potential evidence.

   Acceptance Test:
   The already calculated hash values of the known set of files should be compared with the suspicious files
   and should they match with the values of some or more such files, the matched files are potential evidence

3.
   Given a hash list consisting of unique hash values for known miscreant sourced / malicious files
   and a list of files in a directory which are potentially harmful
   when the hash values can be generated for the files to be tested
   and checked against the list to determine the files to be black listed
   then the files of which the hash values matches can be considered as potential evidence

   Acceptance Test:
   If the hash values already present in the black list match with any of the hash values of the files being tested
   then those files are black listed

4.
   Given a hash list consisting of unique hash values for known trustworthy files
   and a list of files in a directory which are potentially harmful
   when the hash values can be generated for the files to be tested
   and checked against the list to determine the files to be white listed
   then the files of which the hash values matches can be considered as potential evidence

   Acceptance Test:
   If the hash values already present in the white list match with any of the hash values of the files being tested
   then those files are trustworthy

5.
   Given a list of already generated trusted hash values of installation files or configuration setup
   and a list of new files or similar files which do not already belong to the list
   when the has values of the new files are generated and scanned with the list of scanned files
   then the files of which the hash values do not match can be considered untrustworthy or potentially harmful

   Acceptance Test:
   If the hash values present in the list of pre-installed, pre-configured files match with any of the hash values of
   the files being tested then those files are the same installation/configuration files and haven't been modified.

6.
   Given a list of hash values of the trusted system or network files
   and a set of new incoming files that needs to be installed or processed in system or in network
   when the hash is generated for the incoming foreign files
   then the trusted set of files can act as a filter to restrict the entry of any malicious files
   and added to the black listed files list

   Acceptance Test:
   If the hash values of the trusted files match with the hash values of the incoming files, then it proves
   that the files have not been modified and are safe for again entering the system.


7.
   Given a list of hash values for the files belonging to a secured network
   and the routers and firewall perform periodic checks on the files
   when the hash values are recalculated
   then they should match with the hash values generated earlier in order to confirm that they have not been
   modified in any way

   Acceptance Test:
   If the hash values of the files originated or belonging to a secured network when sent from
   its server matches with the files received from the client end, then the files have not been modified or changed,
   but if it does then the firewall should perform checks in order to confirm if the file is
   not malicious

Forensics Searching:

8.
   Given a list of documents in a directory
   and a specific document has to be searched from the list
   When a query is passed to the system with a keyword relating
   to a specific context or category
   then the document is searched from the directory

   Acceptance Test:
   The program should be able to search for the keyword or phrase searched by the user from the given list
   of documents in a directory


9.
   Given a list of documents in a directory
   and specific contents has to be searched within the document(s)
   when the search query is passed to the program
   and content search is performed
   then the document containing that specific content from the directory should be found if it exists.

   Acceptance Test:
   The contents should be searched and displayed if they exist in any of the documents present in the list
   in the given directory

10.
   Given a list of documents in a directory
   and the date created, modified, last accessed, number of times accessed has to be searched
   when the search query for a specific document or a keyword or phrase in a document is passed to the program
   and the search is performed
   then the document's details or the document with the specific content's details should be displayed if the document
   or any of its contents exists

   Acceptance Test:
   The details about the creation, modification, last accessed, number of times accessed
   for the document containing the specific contents searched or the document itself
   should be searched and displayed if they exist in any of the documents present in the list
   in the given directory

Forensics Indexing:

11.
   Given a list of documents in a directory
   and if a disk image has to be created
   when the indexing is performed
   then the disk image is created based on the indexing and stored in a text file

   Acceptance Test:
   The indexing should be able to generate an accurate disk image of all the information stored in it at various
   index locations and stored in a text file

12.
   Given a list of documents in a directory
   and if a memory snapshot has to be prepared
   when the indexing is performed
   then the memory snapshot is created with a time stamp to which a use can refer to
   and the user can rollback to the memory snapshot at any given time

   Acceptance Test:
   The indexing should be able to generate the memory snapshot with a specific time stamp
   of the data present on the drive of system
   or the server and the user should be able to access the memory snapshot corresponding to
   a particular time stamp

13.
   Given a list of documents in a directory
   and a network trace has to be generated
   when the indexing is performed
   then the network trace should be generated along side to monitor any errors or issues
   and provides access to the user for the information about method invocations
   and network traffic generated by a managed application

   Acceptance Test:
   The system should be able to generate the network trace when the indexing is done for the
   activities performed with respective time stamps in order for the user to evaluate them at a later stage

14.
   Given a list of documents in a directory
   and a specific keyword is searched in that list
   when the search and indexing is performed
   then the the offset of where each string was found in the target file should be displayed
   and the result should be stored in a text file

   Acceptance Test:
   The offset should be found where each searched string was found in the target file and printed and
   stored exactly in the text file

15.
   Given a list of documents in a directory
   and a specific keyword is searched in that list
   when the search and indexing is performed
   then the the offset of where each string was found in the target file should be displayed
   and the result should be stored in a CSV file

   Acceptance Test:
   The offset should be found where each searched string was found in the target file and printed and
   stored exactly in the CSV file

16.
   Given a list of documents in a directory
   and a specific keyword is searched in that list
   when the search and indexing is performed
   then the the offset of where each string was found in the target file should be displayed
   and the results should be stored in a PDF file

   Acceptance Test:
   The offset should be found where each searched string was found in the target file and printed and
   stored exactly in the PDF file

17.
   Given a list of documents in a directory
   and a list of legitimate words are to be searched that are conforming to the law or to rules
   when the indexing is performed
   then the strings which have high probability of being words are found
   through a numeric weight calculated for each word, comparing it to a matrix of weights in a dictionary
   and stored in a text file

   Acceptance Test:
   The indexing should calculate the numeric weights and compare them against the matrix of weights in a dictionary
   and find the strings having highest probability likely to be legitimate words and store them in a text file

18.
   Given a list of documents in a directory
   and a list of legitimate words are to be searched that are conforming to the law or to rules
   when the indexing is performed
   then the strings which have high probability of being words are found
   through a numeric weight calculated for each word, comparing it to a matrix of weights in a dictionary
   and stored in a PDF file

   Acceptance Test:
   The indexing should calculate the numeric weights and compare them against the matrix of weights in a dictionary
   and find the strings having highest probability likely to be legitimate words and store them in a PDF file

19.
   Given a list of documents in a directory
   and a list of legitimate words are to be searched that are conforming to the law or to rules
   when the indexing is performed
   then the strings which have high probability of being words are found
   through a numeric weight calculated for each word, comparing it to a matrix of weights in a dictionary
   and stored in a CSV file.

   Acceptance Test:
   The indexing should calculate the numeric weights and compare them against the matrix of weights in a dictionary
   and find the strings having highest probability likely to be legitimate words and store them in a CSV file

20.
   Given a list of documents in a directory
   and a list of legitimate words are to be sorted
   when the indexing is performed
   then the strings which have high probability of being words are found
   through a numeric weight calculated for each word, comparing it to a matrix of weights in a dictionary
   and sorting is performed
   and the sorted list of legitimate words is stored in a text file

   Acceptance Test:
   The indexing should calculate the numeric weights and compare them against the matrix of weights in a dictionary
   and find the strings having highest probability likely to be legitimate words and store them in a sorted manner
   in a text file


21.
   Given a list of documents in a directory
   and a list of legitimate words are to be sorted
   when the indexing is performed
   then the strings which have high probability of being words are found
   through a numeric weight calculated for each word, comparing it to a matrix of weights in a dictionary
   and sorting is performed
   and the sorted list of legitimate words is stored in a CSV file

   Acceptance Test:
   The indexing should calculate the numeric weights and compare them against the matrix of weights in a dictionary
   and find the strings having highest probability likely to be legitimate words and store them in a sorted manner
   in a CSV file

22.
   Given a list of documents in a directory
   and a list of legitimate words are to be sorted
   when the indexing is performed
   then the strings which have high probability of being words are found
   through a numeric weight calculated for each word, comparing it to a matrix of weights in a dictionary
   and sorting is performed
   and the sorted list of legitimate words is stored in a PDF file.

   Acceptance Test:
   The indexing should calculate the numeric weights and compare them against the matrix of weights in a dictionary
   and find the strings having highest probability likely to be legitimate words and store them in a sorted manner
   in a PDF file

23.
   Given a list of documents in a directory
   and the duplicate words are to be removed from the contents of the document
   when the indexing is performed
   then the strings which have high probability of being words are found
   through a numeric weight calculated for each word, comparing it to a matrix of weights in a dictionary
   and sorting is performed
   and from the sorted list only unique entries of each legitimate words is stored in a CSV file
   and the rest of the duplicates are removed

   Acceptance Test:
   The indexing should calculate the numeric weights and compare them against the matrix of weights in a dictionary
   and find the strings having highest probability likely to be legitimate words, remove the duplicate words if any
   and store them in a sorted manner in a CSV file

24.
   Given a list of documents in a directory
   and the duplicate words are to be removed from the contents of the document
   when the indexing is performed
   then the strings which have high probability of being words are found
   through a numeric weight calculated for each word, comparing it to a matrix of weights in a dictionary
   and sorting is performed
   and from the sorted list only unique entries of each legitimate words is stored in a text file
   and the rest of the duplicates are removed

   Acceptance Test:
   The indexing should calculate the numeric weights and compare them against the matrix of weights in a dictionary
   and find the strings having highest probability likely to be legitimate words, remove the duplicate words if any
   and store them in a sorted manner in a text file

25.
   Given a list of documents in a directory
   and the duplicate words are to be removed from the contents of the document
   when the indexing is performed
   then the strings which have high probability of being words are found
   through a numeric weight calculated for each word, comparing it to a matrix of weights in a dictionary
   and sorting is performed
   and from the sorted list only unique entries of each legitimate words is stored in a PDF file
   and the rest of the duplicates are removed

   Acceptance Test:
   The indexing should calculate the numeric weights and compare them against the matrix of weights in a dictionary
   and find the strings having highest probability likely to be legitimate words, remove the duplicate words if any
   and store them in a sorted manner in a PDF file

Forensics GPS and EXIF Tags Images Metadata Search:

26.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the image owner (author) or source has to be found
   when the search is performed
   then the exif data tag corresponding to the owner (author) meta data should be traced
   and the result should be stored in a CSV file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to owner or author if it exists and store the results in a CSV file

27.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the image owner (author) or source has to be found
   when the search is performed
   then the exif data tag corresponding to the owner (author) meta data should be traced
   and the result should be stored in a text file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to owner or author it it exists and store the results in a text file

28.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the image owner (author) or source has to be found
   when the search is performed
   then the exif data tag corresponding to the owner (author) meta data should be traced
   and the result should be stored in a PDF file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to owner or author it it exists and store the results in a PDF file


29.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the modified, access, and created times has to be found
   when the search is performed
   then the exif data tag corresponding to the the modified, access, and created times meta data respectively
   should be traced
   and the result should be stored in a PDF file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to the modified, access, and created times if it exists and store the results
   in a PDF file


30.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the modified, access, and created times has to be found
   when the search is performed
   then the exif data tag corresponding to the the modified, access, and created times meta data respectively
   should be traced
   and the result should be stored in a text file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to the modified, access, and created times if it exists and store the results
   in a text file

31.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the modified, access, and created times has to be found
   when the search is performed
   then the exif data tag corresponding to the the modified, access, and created times meta data respectively
   should be traced
   and the result should be stored in a CSV file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to the modified, access, and created times if it exists and store the results
   in a CSV file

32.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the  file size, file path and file storage disk name has to be found
   when the search is performed
   then the exif data tag corresponding to the file size, file path and file storage disk name meta data respectively
   should be traced
   and the result should be stored in a CSV file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to the file size, file path and file storage disk name if it exists
   and store the results in a CSV file

33.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the  file size, file path and file storage disk name has to be found
   when the search is performed
   then the exif data tag corresponding to the the  file size, file path and file storage disk name meta data respectively
   should be traced
   and the result should be stored in a PDF file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to the file size, file path and file storage disk name if it exists
   and store the results in a PDF file

34.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the  file size, file path and file storage disk name has to be found
   when the search is performed
   then the exif data tag corresponding to the the  file size, file path and file storage disk name meta data respectively
   should be traced
   and the result should be stored in a text file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to the file size, file path and file storage disk name if it exists
   and store the results in a text file

35.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the file access attributes such as read-only, system, or archive has to be found
   when the search is performed
   then the exif data tag corresponding to the the file access attributes such as read-only, system, or archive respectively
   should be traced
   and the result should be stored in a text file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to the file access attributes such as read-only, system, or archive if it exists
   and store the results in a text file

36.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the file access attributes such as read-only, system, or archive has to be found
   when the search is performed
   then the exif data tag corresponding to the the file access attributes such as read-only, system, or archive respectively
   should be traced
   and the result should be stored in a CSV file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to the file access attributes such as read-only, system, or archive if it exists
   and store the results in a CSV file

37.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the file access attributes such as read-only, system, or archive has to be found
   when the search is performed
   then the exif data tag corresponding to the the file access attributes such as read-only, system, or archive respectively
   should be traced
   and the result should be stored in a PDF file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to the file access attributes such as read-only, system, or archive if it exists
   and store the results in a PDF file

38.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the image data such as camera type, camera make and camera model has to be found
   when the search is performed
   then the exif data tag corresponding to the image data such as camera type, camera make and camera model respectively
   should be traced
   and the result should be stored in a PDF file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to the camera type, camera make and camera model data if it exists
   and store the results in a PDF file


39.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the image data such as camera type, camera make and camera model has to be found
   when the search is performed
   then the exif data tag corresponding to the image data such as camera type, camera make and camera model respectively
   should be traced
   and the result should be stored in a CSV file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to the camera type, camera make and camera model data if it exists
   and store the results in a CSV file

40.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the image data such as camera type, camera make and camera model has to be found
   when the search is performed
   then the exif data tag corresponding to the image data such as camera type, camera make and camera model respectively
   should be traced
   and the result should be stored in a text file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to the camera type, camera make and camera model data if it exists
   and store the results in a text file

41.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the time and date the image was taken on has to be found
   when the search is performed
   then the exif data tag corresponding to the time and date the image was taken on has to be found respectively
   should be traced
   and the result should be stored in a text file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to the time and date os the image data if it exists
   and store the results in a text file

42.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the time and date the image was taken on has to be found
   when the search is performed
   then the exif data tag corresponding to the time and date the image was taken on has to be found respectively
   should be traced
   and the result should be stored in a CSV file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to the time and date os the image data if it exists
   and store the results in a CSV file

43.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the time and date the image was taken on has to be found
   when the search is performed
   then the exif data tag corresponding to the time and date the image was taken on has to be found respectively
   should be traced
   and the result should be stored in a PDF file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to the time and date os the image data if it exists
   and store the results in a PDF file

44.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the GPS coordinates as in longitude and latitude of the image (where the image was taken) has to be found
   when the search is performed
   then the exif data tag corresponding to the GPS coordinates as in longitude and latitude of the image
   (where the image was taken) has to be found respectively should be traced
   and the result should be stored in a PDF file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to the GPS coordinates as in longitude and latitude data if it exists
   and store the results in a PDF file

45.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the GPS coordinates as in longitude and latitude of the image (where the image was taken) has to be found
   when the search is performed
   then the exif data tag corresponding to the GPS coordinates as in longitude and latitude of the image
   (where the image was taken) has to be found respectively should be traced
   and the result should be stored in a CSV file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to the GPS coordinates as in longitude and latitude data if it exists
   and store the results in a CSV file


46.
   Given a list of file(s) of an image format of Jpg or JPEG in a directory
   containing EXIF (Exchangeable Image File Format) data
   and the GPS coordinates as in longitude and latitude of the image (where the image was taken) has to be found
   when the search is performed
   then the exif data tag corresponding to the GPS coordinates as in longitude and latitude of the image
   (where the image was taken) has to be found respectively should be traced
   and the result should be stored in a text file

   Acceptance Test:
   The EXIF tag locator search should be able to fetch the existing tags in an image file and compare it to its respective
   meta data field to find the tag corresponding to the GPS coordinates as in longitude and latitude data if it exists
   and store the results in a text file


Forensics Time:

47.
   Given a list of documents or files in a directory
   and the time accuracy of all the documents have to found for individual creation, modification
   and deletion operations of the transactions
   when the time accuracy is calculated
   then the time for the creation, modification and deletion operations for each file is required
   and stored in a Text file

   Acceptance Test:
   The time should be displayed in a from of an official source and it should be accurate to
   the precision of the transaction, in appropriate measurement of day, hour, or millisecond and stored
   in a Text file

48.
   Given a list of documents or files in a directory
   and the time accuracy of all the documents have to found for individual creation, modification
   and deletion operations of the transactions
   when the time accuracy is calculated
   then the time for the creation, modification and deletion operations for each file is required
   and stored in a CSV file

   Acceptance Test:
   The time should be displayed in a from of an official source and it should be accurate to
   the precision of the transaction, in appropriate measurement of day, hour, or millisecond and stored
   in a CSV file


49.
   Given a list of documents or files in a directory
   and the time accuracy of all the documents have to found for individual creation, modification
   and deletion operations of the transactions
   when the time accuracy is calculated
   then the time for the creation, modification and deletion operations for each file is required
   and stored in a PDF file

   Acceptance Test:
   The time should be displayed in a from of an official source and it should be accurate to
   the precision of the transaction, in appropriate measurement of day, hour, or millisecond and stored
   in a PDF file


50.
   Given a list of documents or files in a directory
   and the time authentication of all the documents or files have to be found for validating the source
   when the time authentication is validated
   then the source should be from a trusted one
   and should be stored in a text file

   Acceptance Test:
   The source of time is authenticated to a trusted source of time if it is from
   an NMI timing lab, GPS signal, or the NIST long-wave standard time
   signal (WWVB in the United States) and should be stored in a text file


51.
   Given a list of documents or files in a directory
   and the time authentication of all the documents or files have to be found for validating the source
   when the time authentication is validated
   then the source should be from a trusted one
   and should be stored in a CSV file

   Acceptance Test:
   The source of time is authenticated to a trusted source of time if it is from
   an NMI timing lab, GPS signal, or the NIST long-wave standard time
   signal (WWVB in the United States) and should be stored in a CSV file

52.
   Given a list of documents or files in a directory
   and the time authentication of all the documents or files have to be found for validating the source
   when the time authentication is validated
   then the source should be from a trusted one
   and should be stored in a PDF file

   Acceptance Test:
   The source of time is authenticated to a trusted source of time if it is from
   an NMI timing lab, GPS signal, or the NIST long-wave standard time
   signal (WWVB in the United States) and should be stored in a PDF file


53.
   Given a list of documents or files in a directory
   and the time integrity of all the documents or files have to be validated
   when the time integrity is validated
   then the source should not be corrupted and secured
   and results should be stored in a text file

   Acceptance Test:
   The time integrity should be verified by checking the source is secured and
   not be subject to corruption during normal “handling.” If it is corrupted, either
   inadvertently or deliberately, the corruption should be apparent to a third party
   and the results should be stored in a text file


53.
   Given a list of documents or files in a directory
   and the time integrity of all the documents or files have to be validated
   when the time integrity is validated
   then the source should not be corrupted and secured
   and results should be stored in a PDF file

   Acceptance Test:
   The time integrity should be verified by checking the source is secured and
   not be subject to corruption during normal “handling.” If it is corrupted, either
   inadvertently or deliberately, the corruption should be apparent to a third party
   and the results should be stored in a PDF file


54.
   Given a list of documents or files in a directory
   and the time integrity of all the documents or files have to be validated
   when the time integrity is validated
   then the source should not be corrupted and secured
   and results should be stored in a CSV file

   Acceptance Test:
   The time integrity should be verified by checking the source is secured and
   not be subject to corruption during normal “handling.” If it is corrupted, either
   inadvertently or deliberately, the corruption should be apparent to a third party
   and the results should be stored in a CSV file


55.
   Given a list of documents or files in a directory
   and the non-repudiation factor has to be calculated of all the documents or files
   when the non-repudiation is validated
   then the actions done on the documents or files should be bound to its time
   and respective corresponding times of each actions or events of each file should be stored in a CSV file

   Acceptance Test:
   The events or actions of the document should be bound to its time so that the
   association between the events or actions of each document and their respective time cannot be later denied
   and the results should be stored in a CSV file


56.
   Given a list of documents or files in a directory
   and the non-repudiation factor has to be calculated of all the documents or files
   when the non-repudiation is validated
   then the actions done on the documents or files should be bound to its time
   and respective corresponding times of each actions or events of each file should be stored in a text file

   Acceptance Test:
   The events or actions of the document should be bound to its time so that the
   association between the events or actions of each document and their respective time cannot be later denied
   and the results should be stored in a text file


57.
   Given a list of documents or files in a directory
   and the non-repudiation factor has to be calculated of all the documents or files
   when the non-repudiation is validated
   then the actions done on the documents or files should be bound to its time
   and respective corresponding times of each actions or events of each file should be stored in a PDF file

   Acceptance Test:
   The events or actions of the document should be bound to its time so that the
   association between the events or actions of each document and their respective time cannot be later denied
   and the results should be stored in a PDF file

58.
   Given a list of documents or files in a directory
   and the accountability factor has to be calculated of all the documents or files
   when the time accountability is validated
   then the process of authentication, integrity and acquiring time should be linked to respective actions or events
   and respectively

   Acceptance Test:
   The accountability should be the process of authentication, integrity and acquiring time should be
   binding it to each subject action or event and should be accountable so that a third party
   can determine that due process was applied, and that no corruption transpired.

Forensics in Networks:

59.
   Given a network with a number of sockets (hosts)
   and a breach has to be detected
   when the sockets belonging to a trusted sector are checked
   then on encounter of any untrusted socket (host) the breach could have happened
   and that socket (host) should be verified for sending any malicious files into the network
   and the untrusted socket (host) should be pinned and
   the IP address of that socket should be stored in the text file

   Acceptance Test:
   The breach should be detected by verifying any new untrusted socket (host) that might be sending
   in malicious files or documents into the network and the IP of that socket (host) should be stored in
   a text file

60.
   Given a network with a number of sockets (hosts)
   and a breach has to be detected
   when the sockets belonging to a trusted sector are checked
   then on encounter of any untrusted socket (host) the breach could have happened
   and that socket (host) should be verified for sending any malicious files into the network
   and the untrusted socket (host) should be pinned and
   the IP address of that socket should be stored in the CSV file

   Acceptance Test:
   The breach should be detected by verifying any new untrusted socket (host) that might be sending
   in malicious files or documents into the network and the IP of that socket (host) should be stored in
   a CSV file

61.
   Given a network with a number of sockets (hosts)
   and a breach has to be detected
   when the sockets belonging to a trusted sector are checked
   then on encounter of any untrusted socket (host) the breach could have happened
   and that socket (host) should be verified for sending any malicious files into the network
   and the untrusted socket (host) should be pinned and
   the IP address of that socket should be stored in the PDF file

   Acceptance Test:
   The breach should be detected by verifying any new untrusted socket (host) that might be sending
   in malicious files or documents into the network and the IP of that socket (host) should be stored in
   a PDF file

62.
   Given a network with a number of sockets(hosts)
   and each of the sockets are associated with the different insiders(employees) respectively
   belonging to the network
   and insider activities has to be detected
   when the actions of each sockets by the files they are sharing or receiving are recorded or logged
   then every event or action logged can be verified by checking through a file name or time stamp
   and in case of any suspicious activity or malicious file being floated around the
   source socket (host) should be verified and insider having that socket should be investigation and
   the insider with corresponding IP address of that socket should be stored in the text file

   Acceptance Test:
   Each socket tied with an insider respectively should be filtered for sending any
   in malicious files or documents into the network and the IP of that socket (host) should be stored in
   a text file

63.
   Given a network with a number of sockets(hosts)
   and each of the sockets are associated with the different insiders(employees) respectively
   belonging to the network
   and insider activities has to be detected
   when the actions of each sockets by the files they are sharing or receiving are recorded or logged
   then every event or action logged can be verified by checking through a file name or time stamp
   and in case of any suspicious activity or malicious file being floated around the
   source socket (host) should be verified and insider having that socket should be investigation and
   the insider with corresponding IP address of that socket should be stored in the CSV file

   Acceptance Test:
   Each socket tied with an insider respectively should be filtered for sending any
   in malicious files or documents into the network and the IP of that socket (host) should be stored in
   a CSV file

63.
   Given a network with a number of sockets(hosts)
   and each of the sockets are associated with the different insiders(employees) respectively
   belonging to the network
   and insider activities has to be detected
   when the actions of each sockets by the files they are sharing or receiving are recorded or logged
   then every event or action logged can be verified by checking through a file name or time stamp
   and in case of any suspicious activity or malicious file being floated around the
   source socket (host) should be verified and insider having that socket should be investigation and
   the insider with corresponding IP address of that socket should be stored in the PDF file

   Acceptance Test:
   Each socket tied with an insider respectively should be filtered for sending any
   in malicious files or documents into the network and the IP of that socket (host) should be stored in
   a PDF file


64.
   Given a network with a number of trusted sockets (hosts)
   and each of the sockets are known from their root machines
   when a vulnerability assessment is conducted
   and the files or documents being transferred between the sockets are checked
   then the files having any untrusted feature such as type, time stamp or content which is not known
   in the network should be flagged
   and filtered for any malicious contents
   and these should be stored a text file

   Acceptance Test:
   The files or documents being floated in the network should be filtered for any malicious
   contents or unknown signatures and properties not belonging to the network in the past
   and those files or documents should be flagged and stored in a text file


65.
   Given a network with a number of trusted sockets (hosts)
   and each of the sockets are known from their root machines
   when a vulnerability assessment is conducted
   and the files or documents being transferred between the sockets are checked
   then the files having any untrusted feature such as type, time stamp or content which is not known
   in the network should be flagged
   and filtered for any malicious contents
   and these should be stored a CSV file

   Acceptance Test:
   The files or documents being floated in the network should be filtered for any malicious
   contents or unknown signatures and properties not belonging to the network in the past
   and those files or documents should be flagged and stored in a CSV file


66.
   Given a network with a number of trusted sockets (hosts)
   and each of the sockets are known from their root machines
   when a vulnerability assessment is conducted
   and the files or documents being transferred between the sockets are checked
   then the files having any untrusted feature such as type, time stamp or content which is not known
   in the network should be flagged
   and filtered for any malicious contents
   and these should be stored a PDF file

   Acceptance Test:
   The files or documents being floated in the network should be filtered for any malicious
   contents or unknown signatures and properties not belonging to the network in the past
   and those files or documents should be flagged and stored in a PDF file


67.
   Given a network
   and the number of node hosts or sockets has to be detected
   when the ping is sent using an internet control message protocol or ICMP to each possible IP address present in the network
   then the IP addresses that responds gives us information about them being active
   and for how long it took them to return the response
   and the results should be stored in a text file

   Acceptance Test:
   The number of IP Addresses which respond to the ping message and in reasonable
   amount of time are active and belonging to the network
   and the results should be stored in a text file

68.
   Given a network
   and the number of node hosts or sockets has to be detected
   when the ping is sent using an internet control message protocol or ICMP to each possible IP address present in the network
   then the IP addresses that responds gives us information about them being active
   and for how long it took them to return the response
   and the results should be stored in a CSV file

   Acceptance Test:
   The number of IP Addresses which respond to the ping message and in reasonable
   amount of time are active and belonging to the network
   and the results should be stored in a CSV file

69.
   Given a network
   and the number of node hosts or sockets has to be detected
   when the ping is sent using an internet control message protocol or ICMP to each possible IP address present in the network
   then the IP addresses that responds gives us information about them being active
   and for how long it took them to return the response
   and the results should be stored in a PDF file

   Acceptance Test:
   The number of IP Addresses which respond to the ping message and in reasonable
   amount of time are active and belonging to the network
   and the results should be stored in a PDF file

70.
   Given a network
   and a TCP/IP port scan has to be performed
   When the scan is started
   and the network is checked for each unique IP address and a protocol port number is identified by a 16-bit number, commonly known
   as the port number 0-65,535 as the combination of a port number and IP address provides us with a complete address for communication
   then the ports belonging to a specific <port range> and belongs to a definite <category>
   and the ports belonging to the dynamic category can be checked for malicious files or documents

    | Category            | Port Range     |
    | Wellknown ports     | 0-1023         |
    | Registered ports    | 1024-49,151    |
    | Dynamic ports       | 49,152-65,535  |

    Acceptance Test:
    The scan should be able to detect the port range accurately through the right IP address and identify which category they fall in
    according to the defined categories in order to check if the validation of any port is required


71.
    Given a network
    and the network interface is in a Promiscuous Mode
    and the interface is connected to a port that has visibility to all of the packets
    when the packet sniffing is initiated
    then the operation should be able to detect the presence of all the files or documents present in the network
    and display them with their properties
    and store them in a text file.

    Acceptance Test:
    The packet sniffer should be able to detect all files or documents present in the network and display them with
    respective properties and store the list in a text file.



72.
    Given a network
    and the network interface is in a Promiscuous Mode
    and the interface is connected to a port that has visibility to all of the packets
    when the packet sniffing is initiated
    then the operation should be able to detect the presence of all the files or documents present in the network
    and display them with their properties
    and store them in a CSV file.

    Acceptance Test:
    The packet sniffer should be able to detect all files or documents present in the network and display them with
    respective properties and store the list in a CSV file.



72.
    Given a network
    and the network interface is in a Promiscuous Mode
    and the interface is connected to a port that has visibility to all of the packets
    when the packet sniffing is initiated
    then the operation should be able to detect the presence of all the files or documents present in the network
    and display them with their properties
    and store them in a PDF file.

    Acceptance Test:
    The packet sniffer should be able to detect all files or documents present in the network and display them with
    respective properties and store the list in a PDF file.


73.
    Given a network
    and the network interface is in a Promiscuous Mode
    and the interface is connected to a port that has to capture all of the port activities
    and in a periodic manner (days, weeks, months or years)
    when the packet sniffing is initiated
    then the operation should be able to log all the activities in the network
    and display them for a specific period (days, weeks, months or years)
    and store them in a text file.

    Acceptance Test:
    The sniffer should be able to capture all the activities taking place in the network and display them for a
    period and store the list in a text file.

74.
    Given a network
    and the network interface is in a Promiscuous Mode
    and the interface is connected to a port that has to capture all of the port activities
    and in a periodic manner (days, weeks, months or years)
    when the packet sniffing is initiated
    then the operation should be able to log all the activities in the network
    and display them for a specific period (days, weeks, months or years)
    and store them in a CSV file.

    Acceptance Test:
    The sniffer should be able to capture all the activities taking place in the network and display them for a
    period and store the list in a CSV file.

74.
    Given a network
    and the network interface is in a Promiscuous Mode
    and the interface is connected to a port that has to capture all of the port activities
    and in a periodic manner (days, weeks, months or years)
    when the packet sniffing is initiated
    then the operation should be able to log all the activities in the network
    and display them for a specific period (days, weeks, months or years)
    and store them in a PDF file

    Acceptance Test:
    The sniffer should be able to capture all the activities taking place in the network and display them for a
    period and store the list in a PDF file.