import argparse
import os


# Function for passing arguments through the command line while program execution
def command_line():
    parser = argparse.ArgumentParser('gps extractor')

    parser.add_argument('-v', '--verbose', help="enables printing of additional program messages",
                        action='store_true')
    parser.add_argument('-l', '--logPath', type=validate_directory, required=True,
                        help="specify the directory for forensic log output file")
    parser.add_argument('-c ', '--csvPath', type=validate_directory, required=True,
                        help="specify the output directory for the csv file")
    parser.add_argument('-d', '--scanPath', type=validate_directory, required=True,
                        help="specify the directory to scan")

    the_args = parser.parse_args()

    return the_args


# Test case:
def validate_directory(the_dir):

    if not os.path.isdir(the_dir):
        raise argparse.ArgumentTypeError('Directory does not exist')

    # Check if the write operation works for the provided path
    if os.access(the_dir, os.W_OK):
        return the_dir
    else:
        raise argparse.ArgumentTypeError('Directory is not writable')

