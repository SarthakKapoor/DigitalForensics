import logging


# Class with nested function to generate the Forensic log with the timestamp and message warnings for each event
# Invoked on program's execution
class ForensicLog:
    def __init__(self, log_name):
        try:

            logging.basicConfig(filename=log_name, level=logging.DEBUG, format='%(asctime)s %(message)s')
        except:
            print("Forensic Log Initialization Failure ... Aborting")
            exit(0)

    def log_writer(self, log_type, log_message):
        if log_type == "INFO":
            logging.info(log_message)
        elif log_type == "ERROR":
            logging.error(log_message)
        elif log_type == "WARNING":
            logging.warning(log_message)
        else:
            logging.error(log_message)
        return

    def __del__(self):
        logging.info("Logging Shutdown")
        logging.shutdown()
