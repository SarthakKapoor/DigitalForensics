import os
from forensics_image_meta_data_search import command_parser, csv_read_write, exif
from forensics_image_meta_data_search import logging_class

ts = 0
make = 1
model = 2

userArgs = command_parser.command_line()

logPath = userArgs.logPath + "ForensicLog.txt"
oLog = logging_class.ForensicLog(logPath)

oLog.log_writer("INFO", "Scan Started")

csvPath = userArgs.csvPath + "imageResults3.csv"
oCSV = csv_read_write.CSVWriter(csvPath)

scanDir = userArgs.scanPath
try:
    images = os.listdir(scanDir)
except:
    oLog.log_writer("ERROR", "Invalid Directory " + scanDir)
    exit(0)

print("Program Start")

print()

for aFile in images:

    targetFile = scanDir + '\\' + aFile

    if os.path.isfile(targetFile):

        gpsDictionary, exifList = exif.extract_gps_dictionary(targetFile)

        if gpsDictionary:

            d_coordinate = exif.extract_lat_lon(gpsDictionary)

            lat = d_coordinate.get("Lat")
            latRef = d_coordinate.get("LatRef")
            lon = d_coordinate.get("Lon")
            lonRef = d_coordinate.get("LonRef")

            if lat and lon and latRef and lonRef:
                print()
                str(lat) + ',' + str(lon)

                # write one row to the output file
                oCSV.csv_row_writer(targetFile, exifList[ts], exifList[make], exifList[model], latRef, lat, lonRef, lon
                                    )
                oLog.log_writer("INFO", "GPS Data Calculated for :" + targetFile)
            else:
                oLog.log_writer("WARNING", "No GPS EXIF Data for " + targetFile)
        else:
            oLog.log_writer("WARNING", "No GPS EXIF Data for " + targetFile)
    else:
        oLog.log_writer("WARNING", targetFile + " not a valid file")

del oLog
del oCSV

if __name__ == '__main__':
    csv_read_write_VERSION = '1.0'
