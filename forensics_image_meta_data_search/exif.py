from PIL import Image
from PIL.ExifTags import TAGS, GPSTAGS


# Function to check for the file format, embedded gps tags if format is correct and fetch the gps tags
def extract_gps_dictionary(file_name):
    try:
        pil_image = Image.open(file_name)
        exif_data = pil_image._getexif()

    except Exception:

        return None, None

    image_time_stamp = "NA"
    camera_model = "NA"
    camera_make = "NA"

    if exif_data:

        for tag, the_value in exif_data.items():

            tag_value = TAGS.get(tag, tag)

            if tag_value == 'DateTimeOriginal':
                image_time_stamp = exif_data.get(tag)

            if tag_value == "Make":
                camera_make = exif_data.get(tag)

            if tag_value == 'Model':
                camera_model = exif_data.get(tag)

            if tag_value == "GPSInfo":

                gps_dictionary = {}

                for cur_tag in the_value:
                    gps_tag = GPSTAGS.get(cur_tag, cur_tag)
                    gps_dictionary[gps_tag] = the_value[cur_tag]

                basic_exif_data = [image_time_stamp, camera_make, camera_model]

                return gps_dictionary, basic_exif_data

    else:
        return None, None


# Function to fetch the latitude and longitude for the coordinates
def extract_lat_lon(gps):
    if ("GPSLatitude" in gps and "GPSLongitude" in gps and "GPSLatitudeRef" in gps and
            "GPSLatitudeRef" in gps):

        latitude = gps["GPSLatitude"]
        latitude_ref = gps["GPSLatitudeRef"]
        longitude = gps["GPSLongitude"]
        longitude_ref = gps["GPSLongitudeRef"]

        lat = convert_into_degrees(latitude)
        lon = convert_into_degrees(longitude)

        if latitude_ref == "S":
            lat = 0 - lat

        if longitude_ref == "W":
            lon = 0 - lon

        gps_coordinate = {"Lat": lat, "LatRef": latitude_ref, "Lon": lon, "LonRef": longitude_ref}

        return gps_coordinate

    else:
        return None


# Function to convert the gps coordinates into degree format
def convert_into_degrees(gps_coordinate):
    d0 = gps_coordinate[0][0]
    d1 = gps_coordinate[0][1]
    try:
        degrees = float(d0) / float(d1)
    except:
        degrees = 0.0

    m0 = gps_coordinate[1][0]
    m1 = gps_coordinate[1][1]
    try:
        minutes = float(m0) / float(m1)
    except:
        minutes = 0.0

    s0 = gps_coordinate[2][0]
    s1 = gps_coordinate[2][1]
    try:
        seconds = float(s0) / float(s1)
    except:
        seconds = 0.0

    float_coordinate = float(degrees + (minutes / 60.0) + (seconds / 3600.0))

    return float_coordinate
