import logging
import csv

log = logging.getLogger('main.gps_extractor')


# Class and nested function to perform the write operation in the CSV file for the result fields.
class CSVWriter:
    def __init__(self, file_name):
        try:

            self.csvFile = open(file_name, 'w')
            self.writer = csv.writer(self.csvFile, delimiter=',', quoting=csv.QUOTE_ALL)
            self.writer.writerow(('Image Path', 'TimeStamp', 'Camera Make', 'Camera Model', 'Lat Ref', 'Latitude',
                                  'Lon Ref', 'Longitude'))
        except:
            log.error('CSV File Error')

    def csv_row_writer(self, file_name, time_stamp, camera_make, camera_model, lat_ref, lat_value, lon_ref, lon_value
                       ):
        lat_str = '%.8f' % lat_value
        lon_str = '%.8f' % lon_value

        self.writer.writerow(
            (file_name, time_stamp, camera_make, camera_model, lat_ref, lat_str, lon_ref, lon_str))

    def __del__(self):
        self.csvFile.close()
