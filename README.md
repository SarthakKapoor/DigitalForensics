## Standardisation of Digital Forensics Investigation  

## Overview

The field of Digital forensics investigation has faced many challenges in the past recent years, but the fact that there are no existing 
means of finding if the evidence is actually real or not is an important challenge. There is no method present to establish the standards 
to evaluate the existing digital forensic procedures and software tools. Besides, there is no clarity on the methods used for the collection, 
analysis and presentation of the data as evidence. The existing procedures vary significantly due to a lack of an existing digital forensic 
framework involving best practices and principles for investigations in a machine, extensive network or even a cloud since evidence is likely 
to be ephemeral and stored in media beyond the immediate control of an investigator. The project contributes to resolving the challenge through
the development of a BDD framework to standardise the digital forensics investigation procedures. 

## Aim 

The main aim of the project is to address these challenges through developing standards in the form of a methodology or framework conisting of
steps or processes in a variety of scenarios for the industry to follow which could be utilised in the field of Digital Forensics.  

## Instructions

The construction of methodology or framework to allign standards consists of various experimental examples and tests performed in the process to find the
usefulness and their application in different scenarios through a command line python interface.   

## Installation 

1. Install Python version 2.X.x or 3.X.x
2. Setup a Python virtual environment if having multiple Python versions already installed on your machine and select a version which is most suitable. 
3. Install a python IDE for respective OS.
4. Install git and clone the project.
5. Run Setup.py file to install all the packages and modules.
6. Refer to the packages required section or requirements.txt file for any modules or packages not installed during installation, to make sure they exists. 
7. Run CMD or terminal respectively to run the commands. 
8. Run the commands as mentioned in the comments in code.  
9. Take care while running the codes on different sets of documents as mentioned in the comments in code.

### Python version

```
Python 2.X.x or 3.X.x 
```

### Virtual environment

```
Run command: virtualenv and an environment name 
```

### Project setup

```
Run command: python setup.py install 
```

### Packages required

```
behave v1.2.6
ExifRead v2.1.2
Pillow v5.2.0 (for Python 3.X.x)
PIL v1.1.7 (for Python 2.X.x)
```

### Behave setup

```
Run command: pip install behave
```
```
To execute features:
Run command: % behave
```

### Wiki 

```
Project documents
```
