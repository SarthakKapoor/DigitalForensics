Feature: Forensic Searching

  Scenario: Keyword / phrase search and results stored in Text file
    Given Search: A list of documents in a directory
      And Search: A specific document is to be searched from the list
     When Search: A query is passed to the system with a keyword relating to a specific context or category
     Then Search: The document is searched in the directory
      And Search: The index locations where the keyword(s) / phrase(s) was found are printed in a text file

  Scenario: Keyword / phrase search and results stored in CSV file
    Given Search: A series of documents contained in a directory
      And Search: A specific document has to be searched in the list
     When Search: A query is passed to the system with a keyword relating to a specific context or category
     Then Search: The document is searched within the directory
      And Search: The index locations where the keyword(s) / phrase(s) was found are printed in a CSV file

  Scenario: Keyword / phrase search and results stored in PDF file
    Given Search: A list of documents in a directory
      And Search: A specific document has to be searched within the list
     When Search: A query is passed to the system with a keyword relating to a specific context or category
     Then Search: The document is searched from the directory
      And Search: The index locations where the keyword(s) / phrase(s) was found are printed in a PDF file

  Scenario: Content driven search
     Given Search: A documents list in a directory
       And Search: Specific contents has to be searched within the document(s)
      When Search: The search query is passed to the program
       And Search: The search is performed
      Then Search: The document containing that specific content from the directory should be found if it exists

  Scenario: File / Document Meta Data Search
     Given Search: A list of documents in a directory
       And Search: The date created, modified, last accessed, number of times accessed has to be searched
      When Search: The search query for a specific document or a keyword or phrase in a document is passed to the program and the search is performed
       And Search: The search is done
      Then Search: The details of the documents or the document with the specific content details should be displayed if the document or any of its contents exists