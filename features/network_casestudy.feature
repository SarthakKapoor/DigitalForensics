Feature: Forensics Network Case Studies

  Background: The following scenarios are from real life case studies of Forensics in Networks based on actual steps and procedures taken to find the digital evidence and investigate the crime

    Scenario: Insider Activity Scanning | Case Study 1
       Given An organisation in a crisis, with a senior staff downloading many malicious images containing harmful trojan virus
         And That the particular staff member was under suspicion of performing this action
         And There was a need to find evidence ting his machine to the malicious image files in order to prove that he was doing it
         And The forensics personal was given the access to trace the network activities of all employees (insiders)
        When The insider activity scanning was performed the IP address of each employee were checked for the source of the malicious files being brought into the network
        Then The IP address of the administrator also came as a suspect as it was also responsible for transferring files from their PC to the senior staff
         And On further checks of the activities linking to the routine of work showed that it was actually the administrator who was using the senior machine of the member of staff virtually to bring in malicious files in to the system as the administrator had access to all the PC machines of the employees in the organisation
         And A detailed report of all insider activity with the IP addresses and the time stamps of files being transferred including the virtual environment of the administrator activity tying them to the crime was sent to the organisation heads and management to discuss for them to take required legal actions


    Scenario: Vulnerability Assessment Scanning | Case Study 2
       Given A new start-up / SME (small-medium enterprise) with an E-government model has recently begun to notice anomalies in its accounting and product records based in London
         And There are a number of entries which are suspicious when an initial check of system log files has been done
         And IP addresses outside the company firewall with a large amount of data being sent
         And Complaints regarding a strange message being displayed during order processing from a number of customers are recieved
         And Redirection happens to the payment page that does not look legitimate
         And The company makes use of a general purpose eBusiness package (OSCommerce)
         And The company do not feel that they have the expertise to carry out a full scale malware/forensic investigation even though they have a small team of six IT support professionals
         And The company is anxious to ensure that their systems are not being compromised as there is an increased competition in the hi-tech domain
         And To determine whether any malicious activity has taken place, and to ensure that there is no malware within their systems, they employ a digital forensic investigator
        When Vulnerability assessment is conducted and the firewall is investigated
        Then It is found that there were some untrusted sockets (hosts) which were encountered during the scan which previously were not a part of the trusted company network
         And The IP addresses and details of those un-trusted sockets (hosts) are stored in a suitable file format and shared with the management team of the company
         And The respective ports belonging to the un-trusted hosts from where the malicious packets were coming was blocked
         And After making a careful backup of all the essential files on the local drives of system and PCs of each employee a network reset is initiated to erase all malicious files floating in the network is conducted
         And Any malicious files found on the network are permanently deleted from all systems or PCs which might have been infected
         And The files are added to the firewall for future tracing and restriction from entry
         And The security patches are applied by the IT support team on a monthly basis as company uses Windows Server NT for its servers
         And The network of the company is cleaned from any malicious threats

     Scenario: Network Port Scanning | Case Study 3
        Given An employee of an insurance bank turned over their PC machine after leaving the bank from employment
          And The managers suspected that this individual had revealed confidential information regarding loan clients and credit information
          And To inspect email server records for deleted email files that might cast light on the individual’s actions, the Digital Forensics team was brought
         When The network port scan was conducted to find the deleted emails associated with the port of the employee under suspicion
         Then In a very brief time, the IP address was caught associated with the suspected email
          And Deletion activity showing the former employee’s actions of misconduct
          And A detailed report of the employees network activities and the IP address linked was shared with the management of the company in a suitable file format for necessary actions to be taken against the misconduct of the company



