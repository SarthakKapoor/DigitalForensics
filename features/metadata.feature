Feature: Forensics GPS and EXIF Tags Images Metadata Search

  Scenario: Image Owner / Author Source Search and results are stored in a Text file
     Given Metadata: A number of file(s) of an image format of JPG or JPEG in a directory consisting of EXIF (Exchangeable Image File Format) data
       And Metadata: The image owner (author) or source has to be searched
      When Metadata: The search is performed for the image owner(author) name
      Then Metadata: The exif data tag corresponding to the owner (author) meta data should be tracked
       And Metadata: The result should be generated in a Text file

  Scenario: Image Owner / Author Source Search and results are stored in a CSV file
     Given Metadata: A list of image format file(s) either Jpg or JPEG in a directory containing EXIF (Exchangeable Image File Format) data
       And Metadata: The image owner (author) or source has to be identified
      When Metadata: We perform search
      Then Metadata: The exif data tag corresponding to the owner (author) meta data should be matched
       And Metadata: The result should be displayed in a CSV file

  Scenario: Image Owner / Author Source Search and results are stored in a PDF file
     Given Metadata: A directory containing a list of file(s) of an image format of JPG or JPEG with respective EXIF (Exchangeable Image File Format) data
       And Metadata: The image owner (author) or source has to be found
      When Metadata: The search is initiated
      Then Metadata: The exif data tag corresponding to the owner (author) meta data should be traced
       And Metadata: The results should be written in a PDF file

  Scenario: Image modification, access and creation time search and results are stored in a PDF file
     Given Metadata: A list of image format file(s) in JPG or JPEG in a directory containing EXIF (Exchangeable Image File Format) data
       And Metadata: The modified, access, and created times have to be explored
      When Metadata: The meta data search is performed
      Then Metadata: The exif data tag corresponding to the the modified, access, and created times meta data respectively should be found
       And Metadata: The result must be saved in a PDF file format

  Scenario: Image modification, access and creation time search and results are stored in a Text file
     Given Metadata: A directory with a list of image format file(s) in JPG or JPEG containing EXIF (Exchangeable Image File Format data)
       And Metadata: The modified, access, and created times have to be searched
      When Metadata: The meta data search is initiated
      Then Metadata: The exif data tag corresponding to the the modified, access, and created times meta data respectively must be traced
       And Metadata: The result must be stored in a Text file

  Scenario: Image modification, access and creation time search and results are stored in a CSV file
     Given Metadata: A directory containing a list of image format file(s) in a JPG or JPEG containing EXIF (Exchangeable Image File Format data
       And Metadata: The modified, access, and created times has to be found
      When Metadata: Meta data search is performed
      Then Metadata: The exif data tag corresponding to the the modified, access, and created times meta data respectively should be traced
       And Metadata: The result should get saved in a CSV file

  Scenario: Image size, path and storage data search and results are stored in a CSV file
     Given Metadata: A list of file(s) or document(s) of an image format of JPG or JPEG in a directory containing EXIF (Exchangeable Image File Format) data
       And Metadata: The file size, file path and file storage disk name has to be searched
      When Metadata: The meta-data search takes place
      Then Metadata: The exif data tag corresponding to the file size, file path and file storage disk name meta data respectively should be tracked
       And Metadata: The result must be stored in a CSV file

  Scenario: Image size, path and storage data search and results are stored in a Text file
     Given Metadata: A directory containing list of file(s) of an image format of JPG or JPEG with corresponding EXIF (Exchangeable Image File Format) data
       And Metadata: The file size, file path and file storage disk name has to be explored
      When Metadata: Meta-data search takes place
      Then Metadata: The exif data tag corresponding to the file size, file path and file storage disk name meta data respectively should be traced
       And Metadata: The result must be saved in a Text file

 Scenario: Image size, path and storage data search and results are stored in a PDF file
    Given Metadata: A directory having list of file(s) of an image format of JPG or JPEG with EXIF (Exchangeable Image File Format) data
      And Metadata: The file size, file path and file storage disk name has to be found
     When Metadata: Meta-data search is performed
     Then Metadata: The exif data tag corresponding to the file size, file path and file storage disk name meta data respectively should be matched
      And Metadata: The result must be stored in a PDF file

 Scenario: Image file access attributes search and results are stored in a Text file
    Given Metadata: A list of file(s) of an image format of JPG or JPEG in a directory having EXIF (Exchangeable Image File Format) data
      And Metadata: The file access attributes such as read-only, system, or archive have to be located
     When Metadata: Meta-data search is done
     Then Metadata: The exif data tag corresponding to the file access attributes such as read-only, system, or archive respectively should be located
      And Metadata: The result are prepared in a Text file

 Scenario: Image file access attributes search and results are stored in a PDF file
    Given Metadata: A directory having a list of file(s) of an image format of JPG or JPEG with EXIF (Exchangeable Image File Format) data
      And Metadata: The file access attributes such as read-only, system, or archive has to be found
     When Metadata: Image Meta-data search is performed
     Then Metadata: The exif data tag corresponding to the file access attributes such as read-only, system, or archive respectively should be traced
      And Metadata: The result are stored in a PDF file

 Scenario: Image file access attributes search and results are stored in a CSV file
    Given Metadata: A directory containing list of file(s) of an image format of Jpg or JPEG with EXIF (Exchangeable Image File Format) data
      And Metadata: The file access attributes such as read-only, system, or archive has to be searched
     When Metadata: Meta-data search is triggered
     Then Metadata: The exif data tag corresponding to the file access attributes like the read-only, system, or archive respectively should be traced
      And Metadata: The results are kept in a CSV file

 Scenario: Camera type make and model meta data search (stores results in a PDF file)
    Given Metadata: A list of file(s) of an image format of JPG or JPEG in a directory containing EXIF data
      And Metadata: The image data such as camera type, camera make and camera model has to be searched
     When Metadata: Image Meta-data search is triggered
     Then Metadata: The exif data tag corresponding to the image data such as camera type, camera make and camera model respectively should be tracked
      And Metadata: The results must store in a PDF file

 Scenario: Camera type make and model meta data search (stores results in a CSV file)
    Given Metadata: A directory having a list of file(s) of an image format of JPG or JPEG with EXIF (Exchangeable Image File Format) data and the image data such as camera type, camera make and camera model has to be found
      And Metadata: The image data such as camera type, camera make and camera model has to be found
     When Metadata: Image Meta-data search is conducted
     Then Metadata: The exif data tag corresponding to the image data such as camera type, camera make and camera model respectively should be traced
      And Metadata: The results must save in a CSV file

 Scenario: Camera type make and model meta data search (stores results in a Text file)
    Given Metadata: A directory containing a list of file(s) of an image format of JPG or JPEG with EXIF (Exchangeable Image File Format) data
      And Metadata: The image data such as camera type, camera make and camera model has to be identified
     When Metadata: Image Meta-data search is initiated
     Then Metadata: The exif data tag corresponding to the image data such as camera type, camera make and camera model respectively must be found
      And Metadata: The results are kept in a Text file

 Scenario: Image time and date search (stores results in a Text file)
    Given Metadata: A list of file(s) of an image format of JPG or JPEG in a directory and Containing EXIF (Exchangeable Image File Format) data
      And Metadata: The time and date the image was taken on has to be searched
     When Metadata: Image Meta-data search begins
     Then Metadata: The exif data tag corresponding to the time and date the image was taken on has to be found respectively must be searched
      And Metadata: The result should be stored in a Text file format

 Scenario: Image time and date search (stores results in a PDF file)
    Given Metadata: A directory with a list of file(s) of an image format of JPG or JPEG and Containing EXIF (Exchangeable Image File Format) data
      And Metadata: The time and date the image was taken on has to be identified
     When Metadata: On Image Meta-data search
     Then Metadata: The exif data tag corresponding to the time and date the image was taken on has to be found respectively must be traced
      And Metadata: The result should be stored in a PDF file format

 Scenario: Image time and date search (stores results in a CSV file)
    Given Metadata: A directory having a list of file(s) of an image format of JPG or JPEG and Containing EXIF (Exchangeable Image File Format) data
      And Metadata: The time and date the image was taken on has to be found
     When Metadata: On search of Image Meta-data
     Then Metadata: The exif data tag corresponding to the time and date the image was taken on has to be found respectively should be traced
      And Metadata: The result should be stored in a CSV file format

 Scenario: Image GPS coordinates search(stores results in a PDF file)
    Given Metadata: A list of file(s) of an image format of Jpg or JPEG in a directory containing EXIF (Exchangeable Image File Format) data
      And Metadata: The GPS coordinates as in longitude and latitude of the image (where the image was taken) has to be searched
     When Metadata: On performing Image Meta-data search
     Then Metadata: The exif data tag corresponding to the GPS coordinates as in longitude and latitude of the image (where the image was taken) has to be found respectively should be tracked
      And Metadata: The result should be stored in a file format of PDF file

 Scenario: Image GPS coordinates search(stores results in a CSV file)
    Given Metadata: A list of file(s) of an image format of Jpg or JPEG in a directory consisting of EXIF (Exchangeable Image File Format) data
      And Metadata: The GPS coordinates as in longitude and latitude of the image (where the image was taken) has to be found
     When Metadata: On performing Meta-data search
     Then Metadata: The exif data tag corresponding to the GPS coordinates as in longitude and latitude of the image (where the image was taken) has to be found respectively should be searched
      And Metadata: The result should be stored in a format of the CSV file

 Scenario: Image GPS coordinates search(stores results in a Text file)
    Given Metadata: A list of file(s) of an image format of JPG or JPEG in a directory with EXIF (Exchangeable Image File Format) data present
      And Metadata: The GPS coordinates as in longitude and latitude of the image (where the image was taken has to be found
     When Metadata: On Meta-data search
     Then Metadata: The exif data tag corresponding to the GPS coordinates as in longitude and latitude of the image (where the image was taken) has to be found respectively should be traced
      And Metadata: The result should be stored in a format of Text file
