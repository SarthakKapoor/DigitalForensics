Feature: Time Forensics

 Scenario: File Transaction Manipulation Time and results stored in a Text File
    Given Time: A list of document(s) or files in a directory
      And Time: The time accuracy of all the documents are to found for individual creation, modification and deletion operations of the transactions
     When Time: On calculation of time accuracy
     Then Time: The time for the creation, modification and deletion operations for all files are required
      And Time: The result is stored in a Text file

  Scenario: File Transaction Manipulation Time (results stored in a CSV File)
     Given Time: A directory comprising a list of documents or files
       And Time: The time accuracy of all the documents have to found for individual creation, modification and deletion operations of any transactions
      When Time: The time accuracy is calculated
      Then Time: The time for the creation, modification and deletion operations for each file is required
       And Time: The result is stored in a CSV file

  Scenario: File Transaction Manipulation Time (results stored in a PDF File)
     Given Time: A directory with a list of documents or files
       And Time: The time accuracy of all the documents have to found for individual creation, modification and deletion operations of the transactions
      When Time: The time accuracy is verified
      Then Time: The time for the creation, modification and deletion operations for each file is required and stored in a PDF file
       And Time: The result is stored in a PDF file

  Scenario: Time Authentication (results stored in a text file)
     Given Time: A directory containing a list of documents or files
       And Time: The time authentication of the document(s) or file(s) have to be found for validating the source
      When Time: The time authentication is validated
      Then Time: The source should be from a trusted source
       And Time: The result should be stored in a Text file

  Scenario: Time Authentication (results stored in a CSV file)
     Given Time: A list of document(s) / file(s) in a directory
       And Time: The documents or files have to be found for validating the source
      When Time: On validation of time authentication
      Then Time: The source should be from a trusted one
       And Time: The result should be stored in a CSV file

  Scenario: Time Authentication (results stored in a PDF file)
     Given Time: A directory having a list of documents
       And Time: The time authentication of all the documents or files have to be found for validating the source
      When Time: Time authentication is validated
      Then Time: The source should be from a trusted one and should be stored in a PDF file
       And Time: The result should be stored in a PDF file

 Scenario: Time Integrity (results stored in a text file)
    Given Time: A list of files in a directory
      And Time: The time integrity of all the documents or files have to be checked
     When Time: The time integrity is validated
     Then Time: The source must not be corrupted and secured
      And Time: The results should be stored in a Text file

 Scenario: Time Integrity (results stored in a CSV file)
    Given Time: A list of documents / files in a directory
      And Time: The time integrity of all the documents or files are to be validated
     When Time: Time integrity is validated
     Then Time: The source should not be corrupted
      And Time: The results should be stored in a CSV file

 Scenario: Time Integrity (results stored in a PDF file)
    Given Time: A list of docs in a directory
      And Time: The time integrity of all the documents or files have to be validated
     When Time: On validation of time integrity
     Then Time: The source should not be corrupted and secured
      And Time: The results should be stored in a PDF file

 Scenario: Non-repudiation (results stored in a text file)
    Given Time: A list of documents or files within a directory
      And Time: The non-repudiation factor has to be verified of all the documents or files
     When Time: The non-repudiation is validated
     Then Time: The actions done on the documents or files should be bound to the time
      And Time: The respective corresponding times of each actions or events of each file should be stored in a text file

 Scenario: Non-repudiation (results stored in a CSV file)
    Given Time: A directory containing list of documents or files
      And Time: The non-repudiation factor is to be calculated of all the documents or files
     When Time: Non-repudiation is validated
     Then Time: The actions done on the documents or files should be bound with its time
      And Time: The respective corresponding times of each actions or events of each file should be stored in a CSV file

 Scenario: Non-repudiation (results stored in a PDF file)
    Given Time: A list of documents or files in a directory
      And Time: The non-repudiation factor has to be calculated of all the documents or files
     When Time: The validation of non-repudiation takes place
     Then Time: The actions done on the documents or files should be bound to its time
      And Time: The respective corresponding times of each actions or events of each file should be stored in a PDF file

 Scenario: Overall Time Accountability Verification
    Given Time: A directory with a list of files or documents
      And Time: The accountability factor has to be calculated of all the documents or files
     When Time: The time accountability is validated
     Then Time: The process of authentication, integrity and acquiring time should be linked to respective actions or events respectively


