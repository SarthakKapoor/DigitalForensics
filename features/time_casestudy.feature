Feature: Forensics Time Case Studies

    Background: The following scenarios are from real life case studies of Forensics Time units based on actual steps and procedures taken to find the digital evidence and investigate the crime

      Scenario: Forensics Time and Date of (X) sending the email has to be verified | Case Study 1
         Given The marketing manager (X) of an organisation (A) gives a months notice
           And X leaves Organisation (A) receives advice from a number of clients that they received emails from an unknown Gmail account containing defamatory information about Organisation (A)
           And The background information that the Computer Forensics company or personal is asked to search for evidence on PC of (X) that the mail originated from it
           And At the briefing, the Computer Forensics company is given access to PC machine of (X) and PC hard disk of(X)
           And Every bit of data is acquired and preserved by the organisation using procedures
           And The data was analysed in detail and deleted files are recovered including the system files
          When The exact time and date of the creation of email when X was known to be operating the PC is searched
          Then The email with the exact date and time is found
           And The last 3 days of routine of (X) at (A) displayed 1 migrate to your device data file and 1 Microsoft Access File transferred to a USB drive
           And The detailed report of activities of (X) including the time stamp and date of each action was logged
           And The report was submitted to the organisation (A) for discussion and actions to be taken after recommendations from a legal advisor against (X)

      Scenario: Forensics Time and Date of disk being formatted has to be found | Case Study 2
         Given Employee (A) is found stealing product from a super market during lunch break
           And The employee (A) is asked to collect their personal belongings from his office
           And The report to the accountant in 30 minutes time for their final pay reconciliation
           And The following day their company laptop is inspected
           And The PC machine of (A) is found to have been formatted
           And The PC contained important time-sensitive company data that was in My Documents and not part of the regular network backup
           And The Digital Forensics Company or personal is contacted and the briefing as to the types of files required is provides
          When The file recovery operation on the PC is conducted to find the formatted files with the exact time stamps and date and the required files are successfully recovered
          Then The complete suite of data and has absolutely ascertained that the formatting took place when Employee (A) was known to be in the office collecting personal items
           And The company seeks legal advice regarding the appropriate action to take because of the malicious deletion activities

      Scenario: Forensics Time Integrity | Case Study 3
          Given A criminal defence investigation with the log files of an instant message conversations from a mobile phone
            And The log is being used as evidence against the individual were not accurate representations of the original conversation
            And The digital forensics team is asked to perform a thorough evaluation and test of the document and the system for the authenticity of the log
           When Time integrity is validated for checking the exact time stamps for the exchange of each conversations
           Then It is found that there were very small detail anomalies that were present acting as evidence that the documents were not the original log files
            And A detailed log with actual and exact time stamps of the conversation were produced
            And The generated log is stored in a suitable file format for further discussion by the jury and law enforcement

      Scenario: File Transaction Manipulation Time and Time Authentication | Case Study 4
          Given A seller of a business after the deal was called off by the buyer at the last minute to inform allegedly by calling off the sale for reasons other than those allowed in the letter of intent that buyer breached the letter of intent
            And The Buyer provided an Excel spreadsheet that allegedly was created immediately preceding the Buyer’s decision to terminate the sale as the seller alleged that the evidence of the buyer’s true intents
            And The seller alleged that the Buyer withdrew from the sale for other, non-permissible reasons
            And The the digital forensics team was brought in to investigate the matter
           When The File Transaction Manipulation Time and Time Authentication of the the computer system of the Buyer are verified
           Then The spreadsheet that was offered as evidence by buyer was found to be created several months after the decision to terminate the sale
            And The details of the actual time of creation and any changes (modification) were presented in a suitable file format for the seller to discuss and take an action accordingly

