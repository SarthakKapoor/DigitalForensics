Feature: Forensics Hashing Case Studies

  Background: The following scenarios are from real life case studies of Forensics Hashing based on actual steps and procedures taken to find the digital evidence and investigate the crime

    Scenario: Forensics Hashing case study 1
       Given Several managers left a software design firm within a few weeks
         And The managers started up a new firm, producing similar products in direct competition with original firm
         And Digital Forensics team was engaged to inspect former managers’ computers, which had been erased
        When Forensics hashing is performed to list the original documents or files that were changed and overwritten
        Then The evidence that business plan and designs for new firm were acquired directly from original firm was uncovered
         And A detailed report of the evidence in a suitable format was sent to the management of the firm for presenting in the court
         And Until a sufficient time had passed for them to have produced their own designs, the court stopped the new firm from selling their product
         And The former managers given nine month injunction

    Scenario: Forensics Hashing case study 2
       Given A company has spent years producing a catalogue selling thousands of industry-specific parts
         And The company finds that a competing catalogue with identical drawings and designs had been produced in a few weeks by a new competitor
         And The digital forensics team is engaged to find evidence that the designs of the new company were stolen from the original company
        When The forensics hashing followed by search is performed to find comparisons between the documents
        Then It is found that in fact most documents either have the same digital hash value or have been modified by content to cover their tracks leading back to the main organisation
         And A detailed report of all the documents with their properties are shared with the original company to present in the court
         And After findings are presented to court showing original company’s original artwork
         And The text being used in new catalogue of the new company
         And The new company enjoined from using designs for several months

    Scenario: Forensics Hashing and Time Authentication case study 3
       Given A manager of a real estate fund is accused of increasing the share values of the fund through providing false information to potential investors when the value of the fund plunged
         And The manager is further accused of faking and falsely dating computer documents in order to support claim of innocence
         And The digital forensics team is brought in to find out the actual date and time signatures of the modification of the documents
        When The forensics hashing is conducted and the file creation, access and modification time stamps and dates are found
        Then A detailed report in a suitable file format is shared with the real estate fund company board
         And The information recovered by the digital forensics team is able to mitigate and diminish claims against the client

    Scenario: Forensics Hashing case study 4
       Given An employment dispute of an alleged wrongful termination
         And Their clients have to determine whether any disciplinary documentation have been altered or exposed out of the incident
         And The digital forensics team is brought in to investigate the matter
        When The forensics hashing is performed along with thorough validation
        Then It is found that a particular document that was undertaken by team to validate which was being with held by them
         And The document had been altered after the termination date, that the information that has specifically been altered was consistent with the employer’s sworn testimony
         And A detailed report of the validation performed is sent to the employer in a suitable file format for further discussion and required actions to be taken against the employee

