from behave import *


# Camera type, make, model and GPS coordinates meta data search


@given('An investigation of 2014 case into the child abduction and trafficking')
def step_impl(context):
    pass


@given('There are several identities and geographical locations of the potential suspects')
def step_impl(context):
    pass


@given('The case is included with international and domestic elements and collaboration with law enforcement agencies associated')
def step_impl(context):
    pass


@given('Consultation of the Internet Crimes Against Children task force takes place contributing to their resources to solve the case')
def step_impl(context):
    pass


@given('A Municipality organisation from Minnesota, united states is then tasked with the local investigation for the area')
def step_impl(context):
    pass


@given('A 35-year-old male is arrested as a possible offender')
def step_impl(context):
    pass


@given('The city requests the assistance of Minnesota County Sheriff’s Office Intelligence Unit')
def step_impl(context):
    pass


@given('The digital forensics experts to complete a comprehensive examination of the devices seized during the arrest to provide legitimate evidence')
def step_impl(context):
    pass


@When('The meta data search for camera type, make and models are started for images files acquired from the PC and mobile devices')
def step_impl(context):
    pass


@When('Amongst the devices presented for digital forensic examination is a mobile phone with a 12 mega pixel camera capable of capturing high definition images used for taking pictures of the missing children')
def step_impl(context):
    pass


@then('It is found that many of the images captured by and extracted from the phone contained a pixel resolution of 1080 x 900')
def step_impl(context):
    pass


@then('The metadata directly linked the photos to the device they were first found')
def step_impl(context):
    pass


@then('During the digital forensics examination, the experts collect evidence to support charges for the possession, manufacture, and transmission of child abduction and trafficking as the pictures included adult body parts, specifically hands and images of infants')
def step_impl(context):
    pass


@then('Some of the images were also having the GPS Coordinates which confirmed the area codes from where the children went missing')
def step_impl(context):
    pass


@then('The information presented with the case file indicated that the suspect was a local commercial and residential painter')
def step_impl(context):
    pass


@then('Using a monitor with 42” high definition,  a paint-like substance on the hands in the photos is observed, which correlated to the suspect’s trade as a painter by County Sheriff’s Office detectives, in April 2015, state of Minnesota')
def step_impl(context):
    pass


@then('A high level of detail was witnessed within the fingerprints after removing the pornographic details, investigators submitted five images to the Minnesota County Sheriff’s Office Fingerprint unit with a request for analysis, as detectives further enlarged the pictures on the monitor')
def step_impl(context):
    pass


@then('The argument of “someone else did it” is a common defense in the field of digital forensics because it often is not possible to determine who actually used a device Therefore, the potential of a fingerprint association becomes vitally important')
def step_impl(context):
    pass
