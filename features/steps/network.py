from behave import *


# Network Breach Detection and the results stored in a Text file

@given('Network: A network having a number of sockets (hosts)')
def step_impl(context):
    pass


@given('Network: A breach has to be found')
def step_impl(context):
    pass


@When('Network: The sockets belonging to a trusted sector are verified')
def step_impl(context):
    pass


@then("Network: On encounter of any untrusted socket (host) the breach could have occurred")
def step_impl(context):
    #  assert context.failed is False
    pass


@then('Network: The socket (host) is verified for sending any malicious files in the network')
def step_impl(context):
    pass


@then('Network: The untrusted socket(host) should be tracked')
def step_impl(context):
    pass


@then('Network: The IP address of that socket should be stored in the Text file')
def step_impl(context):
    pass


# Network Breach Detection and the results stored in a CSV file


@given('Network: A network containing a number of sockets (hosts)')
def step_impl(context):
    pass


@given('Network: A breach has to be traced')
def step_impl(context):
    pass


@When('Network: The check of the sockets belonging to a trusted sector takes place')
def step_impl(context):
    pass


@then("Network: On encounter of any untrusted socket (host) the breach could have taken place")
def step_impl(context):
    # assert context.failed is False
    pass


@then('Network: The socket (host) is verified for getting any malicious files into the network')
def step_impl(context):
    pass


@then('Network: The untrusted socket (host) should be found')
def step_impl(context):
    pass


@then('Network: The IP address of that socket should be stored in the CSV file')
def step_impl(context):
    pass


# Network Breach Detection and the results stored in a PDF file

@given('Network: A network with a number of sockets (hosts)')
def step_impl(context):
    pass


@given('Network: A breach has to be detected')
def step_impl(context):
    pass


@When('Network: The sockets belonging to a trusted sector are checked')
def step_impl(context):
    pass


@then("Network: On encounter of any untrusted socket (host) the breach could have happened")
def step_impl(context):
    #  assert context.failed is False
    pass


@then('Network: The socket (host) is verified for sending any malicious files into the network')
def step_impl(context):
    pass


@then('Network: The untrusted socket (host) should be pinned')
def step_impl(context):
    pass


@then('Network: The IP address of that socket should be stored in the PDF file')
def step_impl(context):
    pass


# Insider Activity Scanning and the results are stored in a Text file

@given('Network: A network and a number of sockets(hosts)')
def step_impl(context):
    pass


@given('Network: Each of the sockets are associated with the various insiders(employees) respectively belonging to the network')
def step_impl(context):
    pass


@given('Network: The insider activities are to be detected')
def step_impl(context):
    pass


@When('Network: The actions of each sockets by the files they are sharing or receiving are recorded')
def step_impl(context):
    pass


@then('Network: Every event or action logged can be verified by checking through a file name or time-stamp')
def step_impl(context):
    pass


@then('Network: In case of any suspicious activity or malicious file being floated around the source socket (host) should be checked')
def step_impl(context):
    pass


@then('Network: The insider having that socket should be put forward for investigation')
def step_impl(context):
    pass


@then('Network: The insider with corresponding IP address of that socket should be stored in the Text file')
def step_impl(context):
    pass


# Insider Activity Scanning and the results are stored in a CSV file

@given('Network: A number of sockets(hosts) in a network')
def step_impl(context):
    pass


@given('Network: Each of the sockets are associated with many insiders(employees) respectively belonging to the network')
def step_impl(context):
    pass


@given('Network: The insider activities has to be tracked')
def step_impl(context):
    pass


@When('Network: The actions of each sockets by the files they are sharing or receiving are logged')
def step_impl(context):
    pass


@then('Network: Every event or action logged can be verified by checking through a file name or timestamp')
def step_impl(context):
    pass


@then('Network: In case of a suspicious activity or malicious file being floated around the source socket (host) should be verified')
def step_impl(context):
    pass


@then('Network: The insider with that socket should be investigation')
def step_impl(context):
    pass


@then('Network: The insider with corresponding IP address of that socket should be stored in the CSV file')
def step_impl(context):
    pass


# Insider Activity Scanning and the results are stored in a PDF file


@given('Network: A network with a number of sockets(hosts)')
def step_impl(context):
    pass


@given('Network: Each of the sockets are associated with the different insiders(employees) respectively belonging to the network')
def step_impl(context):
    pass


@given('Network: The insider activities has to be detected')
def step_impl(context):
    pass


@When('Network: The actions of each sockets by the files they are sharing or receiving are recorded or logged')
def step_impl(context):
    pass


@then('Network: Every event or action logged can be verified by checking through a file name or time stamp')
def step_impl(context):
    pass


@then('Network: In case of any suspicious activity or malicious file being floated around the source socket (host) should be verified')
def step_impl(context):
    pass


@then('Network: The insider having that socket should be investigation')
def step_impl(context):
    pass


@then('Network: The insider with corresponding IP address of that socket should be stored in the PDF file')
def step_impl(context):
    pass


# Vulnerability Assessment and results stored in a Text file


@given('Network: A network consisting a number of trusted sockets (hosts)')
def step_impl(context):
    pass


@given('Network: Each of the sockets are known from the respective root machines')
def step_impl(context):
    pass


@When('Network: A vulnerability assessment is performed')
def step_impl(context):
    pass


@When('Network: The files or documents being transferred between the sockets are verified')
def step_impl(context):
    pass


@then('Network: The files having any untrusted feature such as type, the timestamp or content which is not known in the network should be flagged')
def step_impl(context):
    #  assert context.failed is False
    pass


@then('Network: The files are filtered for malicious contents')
def step_impl(context):
    #  assert context.failed is False
    pass


@then("Network: The selected files should be stored a Text file")
def step_impl(context):
    pass


# Vulnerability Assessment and results stored in a CSV file

@given('Network: A network of a number of trusted sockets (hosts)')
def step_impl(context):
    pass


@given('Network: Each of the sockets are known from the corresponding root machines')
def step_impl(context):
    pass


@When('Network: A vulnerability assessment is done')
def step_impl(context):
    pass


@When('Network: The files or documents being transferred between the sockets are run through checks')
def step_impl(context):
    pass


@then('Network: The files having any untrusted feature like type, time stamp or content which is not known in the network should be flagged')
def step_impl(context):
    # assert context.failed is False
    pass


@then('Network: The files are checked for any malicious contents')
def step_impl(context):
    #  assert context.failed is False
    pass


@then("Network: The selected files should be stored a CSV file")
def step_impl(context):
    pass


# Vulnerability Assessment and results stored in a PDF file

@given('Network: A network with a number of trusted sockets (hosts)')
def step_impl(context):
    pass


@given('Network: Each of the sockets are known from their root machines')
def step_impl(context):
    pass


@When('Network: A vulnerability assessment is conducted')
def step_impl(context):
    pass


@When('Network: The files or documents being transferred between the sockets are checked')
def step_impl(context):
    pass


@then('Network: The files having any untrusted feature such as type, time stamp or content which is not known in the network should be flagged')
def step_impl(context):
    # assert context.failed is False
    pass


@then('Network: The files are filtered for any malicious contents')
def step_impl(context):
    pass


@then("Network: The selected files should be stored a PDF file")
def step_impl(context):
    pass


# Number of Sockets and results stored in a Text file

@given('Network: A network for examination')
def step_impl(context):
    pass


@given('Network: The number of node hosts or sockets have to be searched')
def step_impl(context):
    pass


@When('Network: The ping is sent using an internet control message protocol to each possible IP address present in the network')
def step_impl(context):
    pass


@then('Network: The IP addresses that responds gives us information regarding the sockets')
def step_impl(context):
    # assert context.failed is False
    pass


@then('Network: The information includes whether sockets are active and time taken for them to return the response')
def step_impl(context):
    # assert context.failed is False
    pass


@then("Network: The results should be stored in a file format of the Text file")
def step_impl(context):
    pass


# Number of Sockets and results stored in a CSV file


@given('Network: A network ready for examination')
def step_impl(context):
    pass


@given('Network: The number of nodes or sockets has to be detected')
def step_impl(context):
    pass


@When('Network: The ping is sent using an ICMP to each possible IP address present in the network')
def step_impl(context):
    pass


@then('Network: The IP addresses which responds gives us information about the sockets')
def step_impl(context):
    pass


@then('Network: The information includes whether sockets are active and the time elapsed for them to return the response')
def step_impl(context):
    pass


@then("Network: The results must be stored in a file format of CSV file")
def step_impl(context):
    pass


# Number of Sockets and results stored in a PDF file


@given('Network: A network')
def step_impl(context):
    pass


@given('Network: The number of node hosts or sockets has to be detected')
def step_impl(context):
    pass


@When('Network: The ping is sent using an internet control message protocol or ICMP to each possible IP address present in the network')
def step_impl(context):
    pass


@then('Network: The IP addresses that responds gives us information about the sockets')
def step_impl(context):
    pass


@then('Network: The information includes whether sockets are active and the time taken for them to return the response')
def step_impl(context):
    pass


@then("Network: The results should be stored in a format of PDF file")
def step_impl(context):
    pass


# Port Scanning

@given('Network: A network and a TCP/IP port scan has to be performed')
def step_impl(context):
    pass


@given('Network: A TCP/IP port scan has to be performed')
def step_impl(context):
    pass


@When('Network: The scan is started')
def step_impl(context):
    pass


@When('Network: The network is checked for each unique IP address')
def step_impl(context):
    pass


@When('Network: A protocol port number is identified by a 16-bit number, commonly known as the port number 0-65,535 as the combination of a port number')
def step_impl(context):
    pass


@When('Network: The IP address provides us with a complete address for communication')
def step_impl(context):
    pass


@then('Network: The ports belonging to a specific <port range> and belongs to a definite <Category>')
def step_impl(context):
    pass


@then('Network: The dynamic category can be checked for malicious files or documents')
def step_impl(context):
    pass


# Port Scanning and result stored in a Text File


@given('Network: A network and network interface is in a Promiscuous Mode')
def step_impl(context):
    pass


@given('Network: The interface is connected to a port that has visibility to all the packets')
def step_impl(context):
    pass


@When('Network: The packet sniffing is started')
def step_impl(context):
    pass


@then('Network: The operation can detect the presence of all the files or documents present in the network')
def step_impl(context):
    pass


@then('Network: The results are stored in a format of Text file')
def step_impl(context):
    pass


# Port Scanning and result stored in a CSV File

@given('Network: A network and the network interface has Promiscuous Mode enabled')
def step_impl(context):
    pass


@given('Network: The interface is connected to a port that has visibility to the present packets')
def step_impl(context):
    pass


@When('Network: The packet sniffing is performed')
def step_impl(context):
    pass


@then('Network: The operation should detect the presence of all the files or documents present in the network')
def step_impl(context):
    pass


@then('Network: The results get stored in a CSV file')
def step_impl(context):
    pass


# Port Scanning and result stored in a PDF File

@given('Network: A network with the network interface in Promiscuous Mode')
def step_impl(context):
    pass


@given('Network: The interface is connected to a port that has visibility to all of the packets')
def step_impl(context):
    pass


@When('Network: The packet sniffing is initiated')
def step_impl(context):
    pass


@then('Network: The operation should be able to detect the presence of all the files or documents present in the network')
def step_impl(context):
    pass


@then('Network: The results are placed in a PDF file')
def step_impl(context):
    pass


# Periodic Network Activity Scan and results stored in Text file

@given('Network: A network having the network interface in Promiscuous Mode')
def step_impl(context):
    pass


@given('Network: The interface is connected to a port that has to captures all port activities')
def step_impl(context):
    pass


@given('Network: The scan is to be performed in a periodic manner (days, weeks, months or years)')
def step_impl(context):
    pass


@When('Network: Packet sniffing begins')
def step_impl(context):
    pass


@then('Network: The operation should log all the activities in the network and display them for a specific period (days, weeks, months or years)')
def step_impl(context):
    pass


@then('Network: The results are stored in a Text file')
def step_impl(context):
    pass


# Periodic Network Activity Scan and results stored in CSV file


@given('Network: A network and the network interface with Promiscuous Mode enabled')
def step_impl(context):
    pass


@given('Network: The interface is connected to a port that has to capture the port activities')
def step_impl(context):
    pass


@given('Network: The scan is performed in a periodic manner (days, weeks, months or years)')
def step_impl(context):
    pass


@When('Network: Packet sniffing is done')
def step_impl(context):
    pass


@then('Network: The operation must log all the activities in the network and display them for a specific period (days, weeks, months or years)')
def step_impl(context):
    pass


@then('Network: The results are stored in a CSV file')
def step_impl(context):
    pass

# Periodic Network Activity Scan and results stored in PDF file

@given('Network: A network and the network interface is in a Promiscuous Mode')
def step_impl(context):
    pass


@given('Network: The interface is connected to a port that has to capture all of the port activities')
def step_impl(context):
    pass


@given('Network: The scan has to be performed in a periodic manner (days, weeks, months or years)')
def step_impl(context):
    pass


@When('Network: Packet sniffing starts')
def step_impl(context):
    pass


@then('Network: The operation should be able to log all the activities in the network and display them for a specific period (days, weeks, months or years)')
def step_impl(context):
    pass


@then('Network: The results are stored in a PDF file')
def step_impl(context):
    #  assert context.failed is False
    pass
