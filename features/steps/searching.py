from behave import *


# Keyword / phrase search and results stored in Text file


@given('Search: A directory containing a list of documents')
def step_impl(context):
    pass


@given('Search: A specific document is to be searched from the list')
def step_impl(context):
    pass


@When('Search: A query is passed to the system with a keyword relating to a specific context or category')
def step_impl(context):
    pass


@then('Search: The document is searched in the directory')
def step_impl(context):
    #  assert context.failed is False
    pass


@then('Search: The index locations where the keyword(s) / phrase(s) was found '
      'are printed in a text file')
def step_impl(context):
    #  assert context.failed is False
    pass


# Keyword / phrase search and results stored in CSV file


@given('Search: A series of documents contained in a directory')
def step_impl(context):
    pass


@given('Search: A specific document has to be searched in the list')
def step_impl(context):
    pass


@When('Search: A query is passed to the system containing a keyword relating to a specific context or category')
def step_impl(context):
    pass


@then('Search: The document is searched within the directory')
def step_impl(context):
    #  assert context.failed is False
    pass


@then('Search: The index locations where the keyword(s) / phrase(s) was found '
      'are printed in a CSV file')
def step_impl(context):
    #  assert context.failed is False
    pass


# Keyword / phrase search and results stored in PDF file

@given('Search: A directory consisting of a list of documents')
def step_impl(context):
    pass


@given('Search: A specific document has to be searched within the list')
def step_impl(context):
    pass


@When('Search: A query is passed to the system having a keyword relating to a specific context or category')
def step_impl(context):
    pass


@then('Search: The document is searched from the directory')
def step_impl(context):
    #  assert context.failed is False
    pass


@then('Search: The index locations where the keyword(s) / phrase(s) was found '
      'are printed in a PDF file')
def step_impl(context):
    #  assert context.failed is False
    pass


# Content driven search


@given('Search: A documents list in a directory')
def step_impl(context):
    pass


@given('Search: Specific contents has to be searched within the document(s)')
def step_impl(context):
    pass


@When('Search: The search query is passed to the program')
def step_impl(context):
    pass


@When('Search: The search is performed')
def step_impl(context):
    pass


@then('Search: The document containing that specific content from the directory should be found if it exists')
def step_impl(context):
    pass


# File / Document Meta Data Search

@given('Search: A list of documents in a directory')
def step_impl(context):
    pass


@given('Search: The date created, modified, last accessed, number of times accessed has to be searched')
def step_impl(context):
    pass


@When('Search: The search query for a specific document or a keyword or phrase in a document is passed to the program '
      'and the '
      'search is performed')
def step_impl(context):
    pass


@When('Search: The search is done')
def step_impl(context):
    pass


@then('Search: The details of the documents or the document with the specific content details should be displayed if '
      'the '
      'document or any of its contents exists')
def step_impl(context):
    pass
