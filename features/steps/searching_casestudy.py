from behave import *


# Rollback to a secured memory snapshot | Case Study 1


@given('A local dairy was robbed')
def step_impl(context):
    pass


@given('The Digital Video Recorder linked to the store’s CCTV system was corrupted which is found on the media file located on the hard disk')
def step_impl(context):
    pass


@given('Provided authorities with no clue as to the identity of the perpetrators')
def step_impl(context):
    pass


@When('Memory snapshot was generated of the time when the hard disk was not corrupted')
def step_impl(context):
    pass


@When('The rollback was made to the time when the drive was functioning alright')
def step_impl(context):
    pass


@When('The data from the non impacted areas of the hard disk were recovered')
def step_impl(context):
    pass


@then('The hard disk was repaired to a state where it could be replayed on PC')
def step_impl(context):
    pass


@then('The potential evidence was traced through the CCTV Footage')
def step_impl(context):
    pass


@then('The identification of the offenders as well as providing incontestable evidence as to their involvement in the crime')
def step_impl(context):
    pass


# Key phrase search and disk image generation | Case Study 2

@given('A SME organisation has been accused of an individual (defendant) of IP theft')
def step_impl(context):
    pass


@given('A restraining order has been issued against the individual for the information potentially stolen')
def step_impl(context):
    pass


@given('An accusation that a defragmentation tool was being run to possibly overwrite the hard disk')
def step_impl(context):
    pass


@given('The digital forensics organisation was brought in for the defendant')
def step_impl(context):
    pass


@When('The information search followed by disk image of the the home PC of individual is created')
def step_impl(context):
    pass


@When('The verified with the testimony given by the opposition')
def step_impl(context):
    pass


@then('The details of the information is displayed and stored in a suitable file sharing format')
def step_impl(context):
    pass


@then('The documents presented by the opposition were in contrast to what was found')
def step_impl(context):
    pass


@then('It was proved that the opposing party was incorrect on facts presented in many forms and had tried to take advantage of the situation')
def step_impl(context):
    pass
