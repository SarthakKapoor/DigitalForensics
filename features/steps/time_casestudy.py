from behave import *


# Forensics Time and Date of (X) sending the email has to be verified | Case Study 1

@given('The marketing manager (X) of an organisation (A) gives a months notice')
def step_impl(context):
    pass


@given('X leaves Organisation (A) receives advice from a number of clients that they received emails from an '
       'unknown Gmail account containing defamatory information about Organisation (A)')
def step_impl(context):
    pass


@given('The background information that the Computer Forensics company or personal is asked to search for evidence on PC of (X) that the mail originated from it')
def step_impl(context):
    pass


@given('At the briefing, the Computer Forensics company is given access to PC machine of (X) and PC hard disk of(X)')
def step_impl(context):
    pass


@given('Every bit of data is acquired and preserved by the organisation using procedures')
def step_impl(context):
    pass


@given('The data was analysed in detail and deleted files are recovered including the system files')
def step_impl(context):
    pass


@When('The exact time and date of the creation of email when X was known to be operating the PC is searched')
def step_impl(context):
    pass


@then('The email with the exact date and time is found')
def step_impl(context):
    pass


@then('The last 3 days of routine of (X) at (A) displayed 1 migrate to your device data file and 1 Microsoft Access File transferred to a USB drive')
def step_impl(context):
    pass


@then('The detailed report of activities of (X) including the time stamp and date of each action was logged')
def step_impl(context):
    pass


@then('The report was submitted to the organisation (A) for discussion and actions to be taken after recommendations from a legal advisor against (X)')
def step_impl(context):
    pass


# Forensics Time and Date of disk being formatted has to be found | Case Study 2

@given('Employee (A) is found stealing product from a super market during lunch break')
def step_impl(context):
    pass


@given('The employee (A) is asked to collect their personal belongings from his office')
def step_impl(context):
    pass


@given('The report to the accountant in 30 minutes time for their final pay reconciliation')
def step_impl(context):
    pass


@given('The following day their company laptop is inspected')
def step_impl(context):
    pass


@given('The PC machine of (A) is found to have been formatted')
def step_impl(context):
    pass


@given('The PC contained important time-sensitive company data that was in My Documents and not part of the regular network backup')
def step_impl(context):
    pass


@given('The Digital Forensics Company or personal is contacted and the briefing as to the types of files required is provides')
def step_impl(context):
    pass


@When('The file recovery operation on the PC is conducted to find the formatted files with the exact time stamps and date and the required files are successfully recovered')
def step_impl(context):
    pass


@then('The complete suite of data and has absolutely ascertained that the formatting took place when Employee (A) was known to be in the office collecting personal items')
def step_impl(context):
    pass


@then('The company seeks legal advice regarding the appropriate action to take because of the malicious deletion activities')
def step_impl(context):
    pass


# Forensics Time Integrity | Case Study 3

@given('A criminal defence investigation with the log files of an instant message conversations from a mobile phone')
def step_impl(context):
    pass


@given('The log is being used as evidence against the individual were not accurate representations of the original conversation')
def step_impl(context):
    pass


@given('The digital forensics team is asked to perform a thorough evaluation and test of the document and the system for the authenticity of the log')
def step_impl(context):
    pass


@When('Time integrity is validated for checking the exact time stamps for the exchange of each conversations')
def step_impl(context):
    pass


@then('It is found that there were very small detail anomalies that were present acting as evidence that the documents were not the original log files')
def step_impl(context):
    pass


@then('A detailed log with actual and exact time stamps of the conversation were produced')
def step_impl(context):
    pass


@then('The generated log is stored in a suitable file format for further discussion by the jury and law enforcement')
def step_impl(context):
    pass


# File Transaction Manipulation Time and Time Authentication | Case Study 4

@given('A seller of a business after the deal was called off by the buyer at the last minute to inform allegedly by calling off the sale for reasons other than those allowed in the letter of intent that buyer breached the letter of intent')
def step_impl(context):
    pass


@given('The Buyer provided an Excel spreadsheet that allegedly was created immediately preceding the Buyer’s decision to terminate the sale as the seller alleged that the evidence of the buyer’s true intents')
def step_impl(context):
    pass


@given('The seller alleged that the Buyer withdrew from the sale for other, non-permissible reasons')
def step_impl(context):
    pass


@given('The the digital forensics team was brought in to investigate the matter')
def step_impl(context):
    pass


@When('The File Transaction Manipulation Time and Time Authentication of the the computer system of the Buyer are verified')
def step_impl(context):
    pass


@then('The spreadsheet that was offered as evidence by buyer was found to be created several months after the decision to terminate the sale')
def step_impl(context):
    pass


@then('The details of the actual time of creation and any changes (modification) were presented in a suitable file format for the seller to discuss and take an action accordingly')
def step_impl(context):
    pass
