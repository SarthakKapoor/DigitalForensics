from behave import *


# Image Owner / Author Source Search and results are stored in a Text file

@given('Metadata: A number of file(s) of an image format of JPG or JPEG in a directory consisting of EXIF (Exchangeable Image File Format) data')
def step_impl(context):
    pass


@given('Metadata: The image owner (author) or source has to be searched')
def step_impl(context):
    pass


@When('Metadata: The search is performed for the image owner(author) name')
def step_impl(context):
    assert True is not False


@then('Metadata: The exif data tag corresponding to the owner (author) meta data should be tracked')
def step_impl(context):
    # assert context.failed is False
    pass


@then('Metadata: The result should be generated in a Text file')
def step_impl(context):
    #  assert context.failed is False
    pass


# Image Owner / Author Source Search and results are stored in a CSV file

@given('Metadata: A list of image format file(s) either Jpg or JPEG in a directory containing EXIF (Exchangeable Image File Format) data')
def step_impl(context):
    pass


@given('Metadata: The image owner (author) or source has to be identified')
def step_impl(context):
    pass


@When('Metadata: We perform search')
def step_impl(context):
    assert True is not False


@then('Metadata: The exif data tag corresponding to the owner (author) meta data should be matched')
def step_impl(context):
    # assert context.failed is False
    pass


@then('Metadata: The result should be displayed in a CSV file')
def step_impl(context):
    #  assert context.failed is False
    pass


# Image Owner / Author Source Search and results are stored in a PDF file

@given('Metadata: A directory containing a list of file(s) of an image format of JPG or JPEG with respective EXIF (Exchangeable Image File Format) data')
def step_impl(context):
    pass


@given('Metadata: The image owner (author) or source has to be found')
def step_impl(context):
    pass


@When('Metadata: The search is initiated')
def step_impl(context):
    pass


@then('Metadata: The exif data tag corresponding to the owner (author) meta data should be traced')
def step_impl(context):
    # assert context.failed is False
    pass


@then('Metadata: The results should be written in a PDF file')
def step_impl(context):
    #  assert context.failed is False
    pass


# Image modification, access and creation time Search and results are stored in a PDF file

@given('Metadata: A list of image format file(s) in JPG or JPEG in a directory containing EXIF (Exchangeable Image File Format) data')
def step_impl(context):
    pass


@given('Metadata: The modified, access, and created times have to be explored')
def step_impl(context):
    pass


@When('Metadata: The meta data search is performed')
def step_impl(context):
    pass


@then('Metadata: The exif data tag corresponding to the the modified, access, and created times meta data respectively should be found')
def step_impl(context):
    pass


@then('Metadata: The result must be saved in a PDF file format')
def step_impl(context):
    pass


# Image modification, access and creation time Search and results are stored in a Text file

@given('Metadata: A directory with a list of image format file(s) in JPG or JPEG containing EXIF (Exchangeable Image File Format data)')
def step_impl(context):
    pass


@given('Metadata: The modified, access, and created times have to be searched')
def step_impl(context):
    pass


@When('Metadata: The meta data search is initiated')
def step_impl(context):
    pass


@then('Metadata: The exif data tag corresponding to the the modified, access, and created times meta data respectively must be traced')
def step_impl(context):
    pass


@then('Metadata: The result must be stored in a Text file')
def step_impl(context):
    #  assert context.failed is False
    pass


# Image modification, access and creation time Search and results in a CSV file

@given('Metadata: A directory containing a list of image format file(s) in a JPG or JPEG containing EXIF (Exchangeable Image File Format data')
def step_impl(context):
    pass


@given('Metadata: The modified, access, and created times has to be found')
def step_impl(context):
    pass


@When('Metadata: Meta data search is performed')
def step_impl(context):
    pass


@then('Metadata: The exif data tag corresponding to the the modified, access, and created times meta data respectively should be traced')
def step_impl(context):
    # assert context.failed is False
    pass


@then('Metadata: The result should get saved in a CSV file')
def step_impl(context):
    pass


# Image size, path and storage data search and results are stored in a CSV file

@given('Metadata: A list of file(s) or document(s) of an image format of JPG or JPEG in a directory containing EXIF (Exchangeable Image File Format) data')
def step_impl(context):
    pass


@given('Metadata: The file size, file path and file storage disk name has to be searched')
def step_impl(context):
    pass


@When('Metadata: The meta-data search takes place')
def step_impl(context):
    pass


@then('Metadata: The exif data tag corresponding to the file size, file path and file storage disk name meta data respectively should be tracked')
def step_impl(context):
    pass


@then('Metadata: The result must be stored in a CSV file')
def step_impl(context):
    pass


# Image size, path and storage data search and results are stored in a Text file


@given('Metadata: A directory containing list of file(s) of an image format of JPG or JPEG with corresponding EXIF (Exchangeable Image File Format) data')
def step_impl(context):
    pass


@given('Metadata: The file size, file path and file storage disk name has to be explored')
def step_impl(context):
    pass


@when('Metadata: Meta-data search takes place')
def step_impl(context):
    pass


@then('Metadata: The exif data tag corresponding to the file size, file path and file storage disk name meta data respectively should be traced')
def step_impl(context):
    pass


@then('Metadata: The result must be saved in a Text file')
def step_impl(context):
    pass


# Image size, path and storage data search and results are stored in a PDF file


@given('Metadata: A directory having list of file(s) of an image format of JPG or JPEG with EXIF (Exchangeable Image File Format) data')
def step_impl(context):
    pass


@given('Metadata: The file size, file path and file storage disk name has to be found')
def step_impl(context):
    pass


@When('Metadata: Meta-data search is performed')
def step_impl(context):
    pass


@then('Metadata: The exif data tag corresponding to the file size, file path and file storage disk name meta data respectively should be matched')
def step_impl(context):
    pass


@then('Metadata: The result must be stored in a PDF file')
def step_impl(context):
    pass


# Image file access attributes search and results are stored in a Text file


@given('Metadata: A list of file(s) of an image format of JPG or JPEG in a directory having EXIF (Exchangeable Image File Format) data')
def step_impl(context):
    pass


@given('Metadata: The file access attributes such as read-only, system, or archive have to be located')
def step_impl(context):
    pass


@When('Metadata: Meta-data search is done')
def step_impl(context):
    pass


@then('Metadata: The exif data tag corresponding to the file access attributes such as read-only, system, or archive respectively should be located')
def step_impl(context):
    # assert context.failed is False
    pass


@then('Metadata: The result are prepared in a Text file')
def step_impl(context):
    pass


# Image file access attributes search and results are stored in a CSV file

@given('Metadata: A directory containing list of file(s) of an image format of Jpg or JPEG with EXIF (Exchangeable Image File Format) data')
def step_impl(context):
    pass


@given('Metadata: The file access attributes such as read-only, system, or archive has to be searched')
def step_impl(context):
    pass


@When('Metadata: Meta-data search is triggered')
def step_impl(context):
    pass


@then('Metadata: The exif data tag corresponding to the file access attributes like the read-only, system, or archive respectively should be traced')
def step_impl(context):
    # assert context.failed is False
    pass


@then('Metadata: The results are kept in a CSV file')
def step_impl(context):
    #  assert context.failed is False
    pass


# Image file access attributes search and results are stored in a PDF file

@given('Metadata: A directory having a list of file(s) of an image format of JPG or JPEG with EXIF (Exchangeable Image File Format) data')
def step_impl(context):
    pass


@given('Metadata: The file access attributes such as read-only, system, or archive has to be found')
def step_impl(context):
    pass


@When('Metadata: Image Meta-data search is performed')
def step_impl(context):
    pass

@then('Metadata: The exif data tag corresponding to the file access attributes such as read-only, system, or archive respectively should be traced')
def step_impl(context):
    pass


@then('Metadata: The result are stored in a PDF file')
def step_impl(context):
    pass


# Camera type make and model meta data search and results are stored in a PDF file


@given('Metadata: A list of file(s) of an image format of JPG or JPEG in a directory containing EXIF data')
def step_impl(context):
    pass


@given('Metadata: The image data such as camera type, camera make and camera model has to be searched')
def step_impl(context):
    pass


@When('Metadata: Image Meta-data search is triggered')
def step_impl(context):
    pass


@then('Metadata: The exif data tag corresponding to the image data such as camera type, camera make and camera model respectively should be tracked')
def step_impl(context):
    # assert context.failed is False
    pass


@then('Metadata: The results must store in a PDF file')
def step_impl(context):
    # assert context.failed is False
    pass


# Camera type make and model meta data search and results are stored in a Text file


@given('Metadata: A directory containing a list of file(s) of an image format of JPG or JPEG with EXIF (Exchangeable Image File Format) data')
def step_impl(context):
    pass


@given('Metadata: The image data such as camera type, camera make and camera model has to be identified')
def step_impl(context):
    pass


@When('Metadata: Image Meta-data search is initiated')
def step_impl(context):
    pass


@then('Metadata: The exif data tag corresponding to the image data such as camera type, camera make and camera model respectively must be found')
def step_impl(context):
    # assert context.failed is False
    pass


@then('Metadata: The results are kept in a Text file')
def step_impl(context):
    #  assert context.failed is False
    pass


# Camera type make and model meta data search and results are stored in a CSV file


@given('Metadata: A directory having a list of file(s) of an image format of JPG or JPEG with EXIF (Exchangeable Image File Format) data and the image data such as camera type, camera make and camera model has to be found')
def step_impl(context):
    pass


@given('Metadata: The image data such as camera type, camera make and camera model has to be found')
def step_impl(context):
    pass


@When('Metadata: Image Meta-data search is conducted')
def step_impl(context):
    pass


@then('Metadata: The exif data tag corresponding to the image data such as camera type, camera make and camera model respectively should be traced')
def step_impl(context):
    # assert context.failed is False
    pass


@then('Metadata: The results must save in a CSV file')
def step_impl(context):
    # assert context.failed is False
    pass


# Image time and date search (stores results in a Text file)

@given('Metadata: A list of file(s) of an image format of JPG or JPEG in a directory and Containing EXIF (Exchangeable Image File Format) data')
def step_impl(context):
    pass


@given('Metadata: The time and date the image was taken on has to be searched')
def step_impl(context):
    pass


@When('Metadata: Image Meta-data search begins')
def step_impl(context):
    pass


@then('Metadata: The exif data tag corresponding to the time and date the image was taken on has to be found respectively must be searched')
def step_impl(context):
    pass


@then('Metadata: The result should be stored in a Text file format')
def step_impl(context):
    pass


# Image time and date search (stores results in a PDF file)

@given('Metadata: A directory with a list of file(s) of an image format of JPG or JPEG and Containing EXIF (Exchangeable Image File Format) data')
def step_impl(context):
    pass


@given('Metadata: The time and date the image was taken on has to be identified')
def step_impl(context):
    pass


@When('Metadata: On Image Meta-data search')
def step_impl(context):
    pass


@then('Metadata: The exif data tag corresponding to the time and date the image was taken on has to be found respectively must be traced')
def step_impl(context):
    pass


@then('Metadata: The result should be stored in a PDF file format')
def step_impl(context):
    pass


# Image time and date search (stores results in a CSV file)


@given('Metadata: A directory having a list of file(s) of an image format of JPG or JPEG and Containing EXIF (Exchangeable Image File Format) data')
def step_impl(context):
    pass


@given('Metadata: The time and date the image was taken on has to be found')
def step_impl(context):
    pass


@When('Metadata: On search of Image Meta-data')
def step_impl(context):
    pass


@then('Metadata: The exif data tag corresponding to the time and date the image was taken on has to be found respectively should be traced')
def step_impl(context):
    pass


@then('Metadata: The result should be stored in a CSV file format')
def step_impl(context):
    pass


# Image GPS coordinates search(stores results in a PDF file)

@given('Metadata: A list of file(s) of an image format of Jpg or JPEG in a directory containing EXIF (Exchangeable Image File Format) data')
def step_impl(context):
    pass


@given('Metadata: The GPS coordinates as in longitude and latitude of the image (where the image was taken) has to be searched')
def step_impl(context):
    pass


@When('Metadata: On performing Image Meta-data search')
def step_impl(context):
    pass


@then('Metadata: The exif data tag corresponding to the GPS coordinates as in longitude and latitude of the image (where the image was taken) has to be found respectively should be tracked')
def step_impl(context):
    pass


@then('Metadata: The result should be stored in a file format of PDF file')
def step_impl(context):
    pass


# Image GPS coordinates search(stores results in a CSV file)

@given('Metadata: A list of file(s) of an image format of Jpg or JPEG in a directory consisting of EXIF (Exchangeable Image File Format) data')
def step_impl(context):
    pass


@given('Metadata: The GPS coordinates as in longitude and latitude of the image (where the image was taken) has to be found')
def step_impl(context):
    pass


@When('Metadata: On performing Meta-data search')
def step_impl(context):
    pass


@then('Metadata: The exif data tag corresponding to the GPS coordinates as in longitude and latitude of the image (where the image was taken) has to be found respectively should be searched')
def step_impl(context):
    # assert context.failed is False
    pass


@then('Metadata: The result should be stored in a format of the CSV file')
def step_impl(context):
    pass


# Image GPS coordinates search and stores results in a Text file

@given('Metadata: A list of file(s) of an image format of JPG or JPEG in a directory with EXIF (Exchangeable Image File Format) data present')
def step_impl(context):
    pass


@given('Metadata: The GPS coordinates as in longitude and latitude of the image (where the image was taken has to be found')
def step_impl(context):
    pass


@When('Metadata: On Meta-data search')
def step_impl(context):
    pass


@then('Metadata: The exif data tag corresponding to the GPS coordinates as in longitude and latitude of the image (where the image was taken) has to be found respectively should be traced')
def step_impl(context):
    #  assert context.failed is False
    pass


@then('Metadata: The result should be stored in a format of Text file')
def step_impl(context):
    pass
