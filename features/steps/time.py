from behave import *


# File Transaction Manipulation Time and results stored in a Text File

@given('Time: A list of document(s) or files in a directory')
def step_impl(context):
    pass


@given('Time: The time accuracy of all the documents are to found for individual creation, modification and Deletion operations of the transactions')
def step_impl(context):
    pass


@When('Time: On calculation of time accuracy')
def step_impl(context):
    pass


@then('Time: The time for the creation, modification and deletion operations for all files are required')
def step_impl(context):
    pass


@then('Time: The result is stored in a Text file')
def step_impl(context):
    # assert context.failed is False
    pass


# File Transaction Manipulation Time and results stored in a CSV File


@given('Time: A directory comprising a list of documents or files')
def step_impl(context):
    pass


@given('Time: The time accuracy of all the documents have to found for individual creation, modification and deletion operations of any transactions')
def step_impl(context):
    pass


@When('Time: The time accuracy is calculated')
def step_impl(context):
    pass


@then('Time: The time for the creation, modification and deletion operations for each file is required')
def step_impl(context):
    pass


@then('Time: The result is stored in a CSV file')
def step_impl(context):
    pass


# File Transaction Manipulation Time and results stored in a PDF File


@given('Time: A directory with a list of documents or files')
def step_impl(context):
    pass


@given('Time: The time accuracy of all the documents have to found for individual creation, modification and deletion operations of the transactions')
def step_impl(context):
    pass


@When('Time: The time accuracy is verified')
def step_impl(context):
    pass


@then('Time: The time for the creation, modification and deletion operations for each file is required and stored in a PDF file')
def step_impl(context):
    pass


@then('Time: The result is stored in a PDF file')
def step_impl(context):
    pass


# Time Authentication and results stored in a Text file


@given('Time: A directory containing a list of documents or files')
def step_impl(context):
    pass


@given('Time: The time authentication of the document(s) or file(s) have to be found for validating the source')
def step_impl(context):
    pass


@When('Time: The time authentication is validated')
def step_impl(context):
    pass


@then('Time: The source should be from a trusted source')
def step_impl(context):
    pass


@then('Time: The result should be stored in a Text file')
def step_impl(context):
    #  assert context.failed is False
    pass


# Time Authentication and results stored in a CSV file


@given('Time: A list of document(s) / file(s) in a directory')
def step_impl(context):
    pass


@given('Time: The documents or files have to be found for validating the source')
def step_impl(context):
    pass


@When('Time: On validation of time authentication')
def step_impl(context):
    pass


@then('Time: The source should be from a trusted one')
def step_impl(context):
    # assert context.failed is False
    pass


@then('Time: The result should be stored in a CSV file')
def step_impl(context):
    #  assert context.failed is False
    pass


# Time Authentication and results stored in a PDF file


@given('Time: A directory having a list of documents')
def step_impl(context):
    pass


@given('Time: The time authentication of all the documents or files have to be found for validating the source')
def step_impl(context):
    pass


@When('Time: Time authentication is validated')
def step_impl(context):
    pass


@then('Time: The source should be from a trusted one and should be stored in a PDF file')
def step_impl(context):
    pass


@then('Time: The result should be stored in a PDF file')
def step_impl(context):
    pass


# Time Integrity and results stored in a Text file

@given('Time: A list of files in a directory')
def step_impl(context):
    pass


@given('Time: The time integrity of all the documents or files have to be checked')
def step_impl(context):
    pass


@When('Time: The time integrity is validated')
def step_impl(context):
    pass


@then('Time: The source must not be corrupted and secured')
def step_impl(context):
    pass


@then('Time: The results should be stored in a Text file')
def step_impl(context):
    #  assert context.failed is False
    pass


# Time Integrity and results stored in a CSV file


@given('Time: A list of documents / files in a directory')
def step_impl(context):
    pass


@given('Time: The time integrity of all the documents or files are to be validated')
def step_impl(context):
    pass


@When('Time: Time integrity is validated')
def step_impl(context):
    pass


@then('Time: The source should not be corrupted')
def step_impl(context):
    pass


@then('Time: The results should be stored in a CSV file')
def step_impl(context):
    pass


# Time Integrity and results stored in a PDF file


@given('Time: A list of docs in a directory')
def step_impl(context):
    pass


@given('Time: The time integrity of all the documents or files have to be validated')
def step_impl(context):
    pass


@when('Time: On validation of time integrity')
def step_impl(context):
    pass


@then('Time: The source should not be corrupted and secured')
def step_impl(context):
    # assert context.failed is False
    pass


@then('Time: The results should be stored in a PDF file')
def step_impl(context):
    pass


# Non-repudiation factor and the results stored in a Text file

@given('Time: A list of documents or files within a directory')
def step_impl(context):
    pass


@given('Time: The non-repudiation factor has to be verified of all the documents or files')
def step_impl(context):
    pass


@When('Time: The non-repudiation is validated')
def step_impl(context):
    pass


@then('Time: The actions done on the documents or files should be bound to the time')
def step_impl(context):
    pass


@then('Time: The respective corresponding times of each actions or events of each file should be stored in a text file')
def step_impl(context):
    pass


# Non-repudiation factor and the results stored in a CSV file

@given('Time: A directory containing list of documents or files')
def step_impl(context):
    pass


@given('Time: The non-repudiation factor is to be calculated of all the documents or files')
def step_impl(context):
    pass


@When('Time: Non-repudiation is validated')
def step_impl(context):
    assert True is not False


@then('Time: The actions done on the documents or files should be bound with its time')
def step_impl(context):
    pass


@then('Time: The respective corresponding times of each actions or events of each file should be stored in a CSV file')
def step_impl(context):
    pass


# Non-repudiation factor and the results stored in a PDF file


@given('Time: A list of documents or files in a directory')
def step_impl(context):
    pass


@given('Time: The non-repudiation factor has to be calculated of all the documents or files')
def step_impl(context):
    pass


@when('Time: The validation of non-repudiation takes place')
def step_impl(context):
    pass


@then('Time: The actions done on the documents or files should be bound to its time')
def step_impl(context):
    # assert context.failed is False
    pass


@then('Time: The respective corresponding times of each actions or events of each file should be stored in a PDF file')
def step_impl(context):
    pass


# Overall Time Accountability Verification

@given('Time: A directory with a list of files or documents')
def step_impl(context):
    pass


@given('Time: The accountability factor has to be calculated of all the documents or files')
def step_impl(context):
    pass


@when('Time: The time accountability is validated')
def step_impl(context):
    pass


@then('Time: The process of authentication, integrity and acquiring time should be linked to respective actions or events respectively')
def step_impl(context):
    # assert context.failed is False
    pass
