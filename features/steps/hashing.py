from behave import *
import os

hash_gen = {}


# Hash Generation and results stored in a CSV

@given('A set of files in a directory')
def step_impl(context):
    hash_gen["input_dir"] = context.text
    pass


@given('An output directory')
def step_impl(context):
    hash_gen["output_dir"] = context.text
    pass


@given('A hashing tool')
def step_impl(context):
    hash_gen["hash_function"] = context.text
    pass


@When('The Forensics hashing is performed')
def step_impl(conetxt):
    print(hash_gen['input_dir'])
    if os.path.isdir(hash_gen["input_dir"]) and os.access(hash_gen['input_dir'], os.R_OK):
        pass


@Then('One way hash of each file in the directory is generated')
def step_impl(context):
    assert os.path.isdir(hash_gen["output_dir"]) is False


@Then('Output should be recorded in a CSV file')
def step_impl(context):
    assert os.access(hash_gen["output_dir"], os.W_OK) is False


@Then('Action log should be generated along')
def step_impl(context):
    assert os.access(hash_gen["output_dir"], os.W_OK) is False


# Hash Generation and results stored in a PDF

@given('A set of files in a valid directory')
def step_impl(context):
    pass


@given('A valid output directory')
def step_impl(context):
    pass


@given('A hashing function')
def step_impl(context):
    pass


@when('We perform the forensics hashing')
def step_impl(context):
    assert True is not False


@Then('One way hash of all the files in the directory is generated')
def step_impl(context):
    # assert context.failed is False
    pass


@Then('Output should be recorded in a PDF file')
def step_impl(context):
    #  assert context.failed is False
    pass


@Then('Action log should be generated')
def step_impl(context):
    # assert context.failed is False
    pass


# Hash Generation and results stored in a Text File

@given('A set of files in a given directory')
def step_impl(context):
    pass


@given('An accessible output directory')
def step_impl(context):
    pass


@given('A hashing method')
def step_impl(context):
    pass


@When('Forensics hashing is conducted')
def step_impl(context):
    assert True is not False


@Then('One way hash of every'
      ' file in the directory is generated')
def step_impl(context):
    assert context.failed is False


@Then('Output should be recorded in a Text File')
def step_impl(context):
    # assert context.failed is False
    pass


@Then('Log should be generated')
def step_impl(context):
    assert context.failed is False


# Forensic Hashing for Evidence Preservation


@given('A set of already known evidence files in a single or multiple directories for investigation')
def step_impl(context):
    pass


@given('A corresponding hash value for each file is present')
def step_impl(context):
    pass


@when('The hash values are compared periodically')
def step_impl(context):
    assert True is not False


@Then("Any suspicious directory on a system could be searched for with the same set of values")
def step_impl(context):
    # assert context.failed is False
    pass


@Then("The matched values could be reported as potential evidence")
def step_impl(context):
    # assert context.failed is False
    pass


# Forensic Hashing for Black Listing


@given('A hash list consisting of unique hash values for known miscreant sourced / malicious files')
def step_impl(context):
    pass


@given('A list of files in a directory which are potentially harmful')
def step_impl(context):
    pass


@when('The hash values can be generated for the files to be tested or verified')
def step_impl(context):
    assert True is not False


@when('Checked against the list to determine the files to be black listed')
def step_impl(context):
    # assert True is not False
    pass


@Then("The files of which the hash values matches can be considered as potential evidence")
def step_impl(context):
    # assert context.failed is False
    pass


# Forensic Hashing for White Listing


@Given('A hash list consisting of hash values uniquely identified for known trustworthy files')
def step_impl(context):
    pass


@Given('A list of files in a directory which are harmful')
def step_impl(context):
    pass


@When('The hash values for the files to be tested are generated')
def step_impl(context):
    assert True is not False


@When('The files are checked against the list to determine the files to be white listed')
def step_impl(context):
    assert True is not False


@Then('The files of which the hash values matches are all potential evidences')
def step_impl(context):
    assert context.failed is False


# Forensic Hashing for Installation / Configuration

@Given('A list of already generated trusted hash values of installation files or configuration setup')
def step_impl(context):
    pass


@Given('A list of new files or similar files which do not already belong to the list')
def step_impl(context):
    pass


@When('The has values of the new files are generated')
def step_impl(context):
    assert True is not False


@When('scanned with the list of scanned files')
def step_impl(context):
    assert True is not False


@Then('The files of which the hash values do not match can be considered untrustworthy or potentially harmful')
def step_impl(context):
    # assert context.failed is False
    pass


# Forensic Hashing for Trusted Network


@Given('A list of hash values of the trusted system or network files')
def step_impl(context):
    pass


@Given('A set of new incoming files that needs to be installed or processed in system or in network')
def step_impl(context):
    pass


@When('The hash is generated for the incoming foreign files')
def step_impl(context):
    assert True is not False


@Then('The trusted set of files can act as a filter to restrict the entry of any malicious files')
def step_impl(context):
    # assert context.failed is False
    pass


@Then('Any malicious files detected are dded to the black listed files list')
def step_impl(context):
    #  assert context.failed is False
    pass

# Forensic Hashing for Firewalls and Secured Network Scans


@Given('A list of hash values for the files belonging to a secured network')
def step_impl(context):
    pass


@Given('The routers and firewall perform periodic checks on the files')
def step_impl(context):
    pass


@When('The hash values are recalculated')
def step_impl(context):
    assert True is not False


@Then('The recalculated hash values should match with the hash values generated earlier in order to confirm that they '
      'have not been modified in any way')
def step_impl(context):
    # assert context.failed is False
    pass
