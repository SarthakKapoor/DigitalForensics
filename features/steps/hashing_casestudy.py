from behave import *


# Forensics Hashing Case Study 1

@given('Several managers left a software design firm within a few weeks')
def step_impl(context):
    pass


@given('The managers started up a new firm, producing similar products in direct competition with original firm')
def step_impl(context):
    pass


@given('Digital Forensics team was engaged to inspect former managers’ computers, which had been erased')
def step_impl(context):
    pass


@When('Forensics hashing is performed to list the original documents or files that were changed and overwritten')
def step_impl(context):
    pass


@then('The evidence that business plan and designs for new firm were acquired directly from original firm was uncovered')
def step_impl(context):
    pass


@then('A detailed report of the evidence in a suitable format was sent to the management of the firm for presenting in the court')
def step_impl(context):
    pass


@then('Until a sufficient time had passed for them to have produced their own designs, the court stopped the new firm from selling their product')
def step_impl(context):
    pass


@then('The former managers given nine month injunction')
def step_impl(context):
    pass


# Forensics Hashing and Search Case Study 2

@given('A company has spent years producing a catalogue selling thousands of industry-specific parts')
def step_impl(context):
    pass


@given('The company finds that a competing catalogue with identical drawings and designs had been produced in a few weeks by a new competitor')
def step_impl(context):
    pass


@given('The digital forensics team is engaged to find evidence that the designs of the new company were stolen from the original company')
def step_impl(context):
    pass


@When('The forensics hashing followed by search is performed to find comparisons between the documents')
def step_impl(context):
    pass


@then('It is found that in fact most documents either have the same digital hash value or have been modified by content to cover their tracks leading back to the main organisation')
def step_impl(context):
    pass


@then('A detailed report of all the documents with their properties are shared with the original company to present in the court')
def step_impl(context):
    pass


@then('After findings are presented to court showing original company’s original artwork')
def step_impl(context):
    pass


@then('The text being used in new catalogue of the new company')
def step_impl(context):
    pass


@then('The new company enjoined from using designs for several months')
def step_impl(context):
    pass


# Forensics Hashing and Time Authentication Case Study 3

@given('A manager of a real estate fund is accused of increasing the share values of the fund through providing false information to potential investors when the value of the fund plunged')
def step_impl(context):
    pass


@given('The manager is further accused of faking and falsely dating computer documents in order to support claim of innocence')
def step_impl(context):
    pass


@given('The digital forensics team is brought in to find out the actual date and time signatures of the modification of the documents')
def step_impl(context):
    pass


@When('The forensics hashing is conducted and the file creation, access and modification time stamps and dates are found')
def step_impl(context):
    pass


@then('A detailed report in a suitable file format is shared with the real estate fund company board')
def step_impl(context):
    pass


@then('The information recovered by the digital forensics team is able to mitigate and diminish claims against the client')
def step_impl(context):
    pass


# Forensic Hashing Case Study 4

@given('An employment dispute of an alleged wrongful termination')
def step_impl(context):
    pass


@given('Their clients have to determine whether any disciplinary documentation have been altered or exposed out of the incident')
def step_impl(context):
    pass


@given('The digital forensics team is brought in to investigate the matter')
def step_impl(context):
    pass


@When('The forensics hashing is performed along with thorough validation')
def step_impl(context):
    pass


@then('It is found that a particular document that was undertaken by team to validate which was being with held by them')
def step_impl(context):
    pass


@then('The document had been altered after the termination date, that the information that has specifically been altered was consistent with the employer’s sworn testimony')
def step_impl(context):
    pass


@then('A detailed report of the validation performed is sent to the employer in a suitable file format for further discussion and required actions to be taken against the employee')
def step_impl(context):
    pass
