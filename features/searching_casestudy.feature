Feature: Forensics Search Case Studies

  Background: The following scenarios are from real life case studies of Forensics search based on actual steps and procedures taken to find the digital evidence and investigate the crime

    Scenario: Rollback to a secured memory snapshot | Case Study 1
       Given A local dairy was robbed
         And The Digital Video Recorder linked to the store’s CCTV system was corrupted which is found on the media file located on the hard disk
         And Provided authorities with no clue as to the identity of the perpetrators
        When Memory snapshot was generated of the time when the hard disk was not corrupted
         And The rollback was made to the time when the drive was functioning alright
         And The data from the non impacted areas of the hard disk were recovered
        Then The hard disk was repaired to a state where it could be replayed on PC
         And The potential evidence was traced through the CCTV Footage
         And The identification of the offenders as well as providing incontestable evidence as to their involvement in the crime

     Scenario: Key phrase search and disk image generation | Case Study 2
        Given A SME organisation has been accused of an individual (defendant) of IP theft
          And A restraining order has been issued against the individual for the information potentially stolen
          And An accusation that a defragmentation tool was being run to possibly overwrite the hard disk
          And The digital forensics organisation was brought in for the defendant
         When The information search followed by disk image of the the home PC of individual is created
          And The verified with the testimony given by the opposition
         Then The details of the information is displayed and stored in a suitable file sharing format
          And The documents presented by the opposition were in contrast to what was found
          And It was proved that the opposing party was incorrect on facts presented in many forms and had tried to take advantage of the situation
