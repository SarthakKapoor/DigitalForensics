Feature: Network Forensics

 Scenario: Network Breach Detection and the results stored in a Text file
    Given Network: A network having a number of sockets (hosts)
      And Network: A breach has to be found
     When Network: The sockets belonging to a trusted sector are verified
     Then Network: On encounter of any untrusted socket (host) the breach could have occurred
      And Network: The socket (host) is verified for sending any malicious files in the network
      And Network: The untrusted socket(host) should be tracked
      And Network: The IP address of that socket should be stored in the Text file

 Scenario: Network Breach Detection and results stored in a CSV file
    Given Network: A network containing a number of sockets (hosts)
      And Network: A breach has to be traced
     When Network: The check of the sockets belonging to a trusted sector takes place
     Then Network: On encounter of any untrusted socket (host) the breach could have taken place
      And Network: The socket (host) is verified for getting any malicious files into the network
      And Network: The untrusted socket (host) should be found
      And Network: The IP address of that socket should be stored in the CSV file

 Scenario: Network Breach Detection and results stored in a PDF file
    Given Network: A network with a number of sockets (hosts)
      And Network: A breach has to be detected
     When Network: The sockets belonging to a trusted sector are checked
     Then Network: On encounter of any untrusted socket (host) the breach could have happened
      And Network: The socket (host) is verified for sending any malicious files into the network
      And Network: The untrusted socket (host) should be pinned
      And Network: The IP address of that socket should be stored in the PDF file

 Scenario: Insider Activity Scanning and results stored in a Text file
    Given Network: A network and a number of sockets(hosts)
      And Network: Each of the sockets are associated with the various insiders(employees) respectively belonging to the network
      And Network: The insider activities are to be detected
     When Network: The actions of each sockets by the files they are sharing or receiving are recorded
     Then Network: Every event or action logged can be verified by checking through a file name or time-stamp
      And Network: In case of any suspicious activity or malicious file being floated around the source socket (host) should be checked
      And Network: The insider having that socket should be put forward for investigation
      And Network: The insider with corresponding IP address of that socket should be stored in the Text file

 Scenario: Insider Activity Scanning and results stored in a CSV file
    Given Network: A number of sockets(hosts) in a network
      And Network: Each of the sockets are associated with many insiders(employees) respectively belonging to the network
      And Network: The insider activities has to be tracked
     When Network: The actions of each sockets by the files they are sharing or receiving are logged
     Then Network: Every event or action logged can be verified by checking through a file name or timestamp
      And Network: In case of a suspicious activity or malicious file being floated around the source socket (host) should be verified
      And Network: The insider with that socket should be investigation
      And Network: The insider with corresponding IP address of that socket should be stored in the CSV file

 Scenario: Insider Activity Scanning and results stored in a PDF file
    Given Network: A network with a number of sockets(hosts)
      And Network: Each of the sockets are associated with the different insiders(employees) respectively belonging to the network
      And Network: The insider activities has to be detected
     When Network: The actions of each sockets by the files they are sharing or receiving are recorded or logged
     Then Network: Every event or action logged can be verified by checking through a file name or time stamp
      And Network: In case of any suspicious activity or malicious file being floated around the source socket (host) should be verified
      And Network: The insider having that socket should be investigation
      And Network: The insider with corresponding IP address of that socket should be stored in the PDF file

 Scenario: Vulnerability Assessment and results stored in a Text file
    Given Network: A network consisting a number of trusted sockets (hosts)
      And Network: Each of the sockets are known from the respective root machines
     When Network: A vulnerability assessment is performed
      And Network: The files or documents being transferred between the sockets are verified
     Then Network: The files having any untrusted feature such as type, the timestamp or content which is not known in the network should be flagged
      And Network: The files are filtered for malicious contents
      And Network: The selected files should be stored a Text file

 Scenario: Vulnerability Assessment and results stored in a CSV file
    Given Network: A network of a number of trusted sockets (hosts)
      And Network: Each of the sockets are known from the corresponding root machines
     When Network: A vulnerability assessment is done
      And Network: The files or documents being transferred between the sockets are run through checks
     Then Network: The files having any untrusted feature like type, time stamp or content which is not known in the network should be flagged
      And Network: The files are checked for any malicious contents
      And Network: The selected files should be stored a CSV file

 Scenario: Vulnerability Assessment and results stored in a PDF file
    Given Network: A network with a number of trusted sockets (hosts)
      And Network: Each of the sockets are known from their root machines
     When Network: A vulnerability assessment is conducted
      And Network: The files or documents being transferred between the sockets are checked
     Then Network: The files having any untrusted feature such as type, time stamp or content which is not known in the network should be flagged
      And Network: The files are filtered for any malicious contents
      And Network: The selected files should be stored a PDF file

 Scenario: Number of Sockets and results stored in a Text file
    Given Network: A network for examination
      And Network: The number of node hosts or sockets have to be searched
     When Network: The ping is sent using an internet control message protocol to each possible IP address present in the network
     Then Network: The IP addresses that responds gives us information regarding the sockets
      And Network: The information includes whether sockets are active and time taken for them to return the response
      And Network: The results should be stored in a file format of the Text file

 Scenario: Number of Sockets and results stored in a CSV file
    Given Network: A network ready for examination
      And Network: The number of nodes or sockets has to be detected
     When Network: The ping is sent using an ICMP to each possible IP address present in the network
     Then Network: The IP addresses which responds gives us information about the sockets
      And Network: The information includes whether sockets are active and the time elapsed for them to return the response
      And Network: The results must be stored in a file format of CSV file

 Scenario: Number of Sockets and results stored in a PDF file
    Given Network: A network
      And Network: The number of node hosts or sockets has to be detected
     When Network: The ping is sent using an internet control message protocol or ICMP to each possible IP address present in the network
     Then Network: The IP addresses that responds gives us information about the sockets
      And Network: The information includes whether sockets are active and the time taken for them to return the response
      And Network: The results should be stored in a format of PDF file

 Scenario: Port Scanning
    Given Network: A network and a TCP/IP port scan has to be performed
      And Network: A TCP/IP port scan has to be performed
     When Network: The scan is started
      And Network: The network is checked for each unique IP address
      And Network: A protocol port number is identified by a 16-bit number, commonly known as the port number 0-65,535 as the combination of a port number
      And Network: The IP address provides us with a complete address for communication
     Then Network: The ports belonging to a specific <port range> and belongs to a definite <Category>
      And Network: The dynamic category can be checked for malicious files or documents

    | Category            | Port Range     |
    | Wellknown ports     | 0-1023         |
    | Registered ports    | 1024-49,151    |
    | Dynamic ports       | 49,152-65,535  |


 Scenario: Port Scanning and results stored in Text file
    Given Network: A network and network interface is in a Promiscuous Mode
      And Network: The interface is connected to a port that has visibility to all the packets
     When Network: The packet sniffing is started
     Then Network: The operation can detect the presence of all the files or documents present in the network
      And Network: The results are stored in a format of Text file


 Scenario: Port Scanning and results stored in CSV file
    Given Network: A network and the network interface has Promiscuous Mode enabled
      And Network: The interface is connected to a port that has visibility to the present packets
     When Network: The packet sniffing is performed
     Then Network: The operation should detect the presence of all the files or documents present in the network
      And Network: The results get stored in a CSV file


 Scenario: Port Scanning and results stored in PDF file
    Given Network: A network with the network interface in Promiscuous Mode
      And Network: The interface is connected to a port that has visibility to all of the packets
     When Network: The packet sniffing is initiated
     Then Network: The operation should be able to detect the presence of all the files or documents present in the network
      And Network: The results are placed in a PDF file


 Scenario: Periodic Network Activity Scan and results stored in Text file
    Given Network: A network having the network interface in Promiscuous Mode
      And Network: The interface is connected to a port that has to captures all port activities
      And Network: The scan is to be performed in a periodic manner (days, weeks, months or years)
     When Network: Packet sniffing begins
     Then Network: The operation should log all the activities in the network and display them for a specific period (days, weeks, months or years)
      And Network: The results are stored in a Text file


 Scenario: Periodic Network Activity Scan and results stored in CSV file
    Given Network: A network and the network interface with Promiscuous Mode enabled
      And Network: The interface is connected to a port that has to capture the port activities
      And Network: The scan is performed in a periodic manner (days, weeks, months or years)
     When Network: Packet sniffing is done
     Then Network: The operation must log all the activities in the network and display them for a specific period (days, weeks, months or years)
      And Network: The results are stored in a CSV file

 Scenario: Periodic Network Activity Scan and results stored in PDF file
    Given Network: A network and the network interface is in a Promiscuous Mode
      And Network: The interface is connected to a port that has to capture all of the port activities
      And Network: The scan has to be performed in a periodic manner (days, weeks, months or years)
     When Network: Packet sniffing starts
     Then Network: The operation should be able to log all the activities in the network and display them for a specific period (days, weeks, months or years)
      And Network: The results are stored in a PDF file