Feature: Forensics Hashing

  Scenario: Hash Generation and results stored in a CSV
     Given A set of files in a directory
         """
         C:\\Users\\Sarthak Kapoor\\Desktop\\DigitalForensics\
         """
       And An output directory
         """
         C:\\Users\\Sarthak Kapoor\\Desktop\\DigitalForensics\
         """
       And A hashing tool
         """
         sha256
         """
      When The Forensics hashing is performed
      Then One way hash of each file in the directory is generated
         """
         C:\\Users\\Sarthak Kapoor\\Desktop\\DigitalForensics\
         """
       And Output should be recorded in a CSV file
         """
         C:\\Users\\Sarthak Kapoor\\Desktop\\DigitalForensics\
         """
       And Action log should be generated
         """
         C:\\Users\\Sarthak Kapoor\\Desktop\\DigitalForensics\
         """

  Scenario: Hash Generation and results stored in a PDF
     Given A set of files in a valid directory
       And A valid output directory
       And A hashing function
      When We perform the forensics hashing
      Then One way hash of all the files in the directory is generated
       And Output should be recorded in a PDF file
       And Action log should be generated

  Scenario: Hash Generation and results stored in a Text File
     Given A set of files in a given directory
       And An accessible output directory
       And A hashing method
      When Forensics hashing is conducted
      Then One way hash of each file in the directory is generated
       And Output should be recorded in a Text File
       And Action log should be generated

  Scenario: Forensic Hashing for Evidence Preservation
     Given A set of already known evidence files in a single or multiple directories for investigation
       And A corresponding hash value for each file is present
      When The hash values are compared periodically
      Then Any suspicious directory on a system could be searched for with the same set of values
       And The matched values could be reported as potential evidence

  Scenario: Forensic Hashing for Black Listing
     Given A hash list consisting of unique hash values for known miscreant sourced / malicious files
       And A list of files in a directory which are potentially harmful
      When The hash values can be generated for the files to be tested or verified
       And Checked against the list to determine the files to be black listed
      Then The files of which the hash values matches can be considered as potential evidence

  Scenario: Forensic Hashing for White Listing
     Given A hash list consisting of hash values uniquely identified for known trustworthy files
       And A list of files in a directory which are harmful
      When The hash values for the files to be tested are generated
       And The files are checked against the list to determine the files to be white listed
      Then The files of which the hash values matches can be considered as potential evidence

  Scenario: Forensic Hashing for Installation / Configuration
     Given A list of already generated trusted hash values of installation files or configuration setup
       And A list of new files or similar files which do not already belong to the list
      When The has values of the new files are generated
       And Scanned with the list of scanned files
      Then The files of which the hash values do not match can be considered untrustworthy or potentially harmful

  Scenario: Forensic Hashing for Trusted Network
     Given A list of hash values of the trusted system or network files
       And A set of new incoming files that needs to be installed or processed in system or in network
      When The hash is generated for the incoming foreign files
      Then The trusted set of files can act as a filter to restrict the entry of any malicious files
       And Any malicious files detected are dded to the black listed files list

  Scenario: Forensic Hashing for Firewalls and Secured Network Scans
     Given A list of hash values for the files belonging to a secured network
       And The routers and firewall perform periodic checks on the files
      When The hash values are recalculated
      Then The recalculated hash values should match with the hash values generated earlier in order to confirm that they have not been modified in any way

