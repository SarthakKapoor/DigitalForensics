Feature: Forensic Indexing

   Scenario: Disk Image Generation and result stored in a Text File
      Given Indexing: A series of documents in a directory
        And Indexing: A disk image has to be generated
       When Indexing: The indexing is performed
       Then Indexing: The disk image is prepared based on the indexing
        And Indexing: Stored in a Text file

   Scenario: Disk Image Generation and result stored in a CSV File
      Given Indexing: A directory accompanied by a list of documents
        And Indexing: A disk image has to be created
       When Indexing: Indexing takes place
       Then Indexing: The disk image is generated based on the indexing
        And Indexing: Stored in a CSV file

   Scenario: Disk Image Generation and result stored in a PDF File
      Given Indexing: A directory possessing a list of documents
        And Indexing: A disk image has to be prepared
       When Indexing: We run indexing
       Then Indexing: The disk image is created based on the indexing
        And Indexing: Stored in a PDF file

   Scenario: Memory Snapshot Generation and result stored in a Text File
      Given Indexing: A list of file(s) in a directory
        And Indexing: A memory snapshot has to be prepared
       When Indexing: The indexing happens
       Then Indexing: The memory snapshot is created with a time stamp to which a user can refer
        And Indexing: The user can rollback to the memory snapshot at any given time
        And Indexing: The results are to be stored in a Text File

   Scenario: Memory Snapshot Generation and result stored in a CSV File
      Given Indexing: A directory with list of documents or files
        And Indexing: A memory snapshot has to be generated
       When Indexing: On performing indexing
       Then Indexing: The memory snapshot is generated with a time stamp to which a user can refer
        And Indexing: The rollback can be performed by the user to the desired memory snapshot at any given time
        And Indexing: The results are stored in CSV File

   Scenario: Memory Snapshot Generation with results stored in a PDF File
      Given Indexing: A list of documents in a drive directory
        And Indexing: A memory snapshot has to be created
       When Indexing: The indexing is started
       Then Indexing: The memory snapshot is prepared with a time stamp to which a use can refer
        And Indexing: The rollback to the memory snapshot at any given time can be performed by the user
        And Indexing: The results are displayed in a PDF File

   Scenario: Forensic Indexing for building a Network Trace
      Given Indexing: A directory and a list of files
        And Indexing: A network trace has to be generated
       When Indexing: The indexing is initiated
       Then Indexing: The network trace should be generated along side to monitor any errors or issues
        And Indexing: Provide access to the user for the information about method invocations
        And Indexing: Network traffic generated by a managed application

   Scenario: Offset search for each string in keyword / Key phrase and result stored in a Text File
      Given Indexing: A set of documents in a directory
        And Indexing: A specific keyword / Key phrase is searched in the list
       When Indexing: The search and indexing is performed
       Then Indexing: The offset of where each string is found in the target file should be displayed
        And Indexing: The result should be stored as a Text file

   Scenario: Offset search for each string in keyword / Key phrase and result stored in a CSV File
      Given Indexing: A directory plus a list of documents
        And Indexing: A specific keyword / Key phrase is searched in the list of documents
       When Indexing: The search and indexing starts
       Then Indexing: The offset for each string in the target file should be displayed
        And Indexing: The result should be saved in a CSV file

   Scenario: Offset search for each string in keyword / Key phrase and result stored in a PDF File
      Given Indexing: A file directory containing a list of documents or files
        And Indexing: A specific keyword / Key phrase is searched in the list of files
       When Indexing: The search and indexing is done
       Then Indexing: The offset of where each string was found in the target file should be displayed
        And Indexing: The result should be saved in a PDF file

   Scenario: Legitimate dictionary words search and result stored in a Text File
      Given Indexing: A directory with a list of files
        And Indexing: A list of legitimate words are to be searched that are laws or rules conforming
       When Indexing: The search and indexing takes place
       Then Indexing: The strings having high probability of being words are found through a numeric weight calculated for each word, comparing it to a matrix of weights in a dictionary
        And Indexing: The result is contained in a Text File

   Scenario: Legitimate dictionary words search and result stored in a CSV File
      Given Indexing: A directory with a series of documents
        And Indexing: A legitimate words list are to be searched that are conforming to the law or to rules
       When Indexing: The search and indexing happens
       Then Indexing: The strings which are having high probability of being words are found through a numeric weight calculated for each word, comparing it to a matrix of weights in a dictionary
        And Indexing: The results are presented in a CSV file

   Scenario: Legitimate dictionary words search and result stored in a PDF File
      Given Indexing: A directory holding a list of documents
        And Indexing: A list of legitimate words are to be searched that are conforming to the law or to rules
       When Indexing: On performing search and indexing
       Then Indexing: The strings with high probability of being words are found through a numeric weight calculated for each word, comparing it to a matrix of weights in a dictionary
        And Indexing: The result are put in a PDF File

   Scenario: Indexed Sorting of Legitimate Words and results stored in Text file
      Given Indexing: A series of files in a directory
        And Indexing: A list of legitimate words to be sorted
       When Indexing: The search and indexing is carried out
       Then Indexing: The strings consisting of high probability of being words are found through a numeric weight calculated for each word, comparing it to a matrix of weights in a dictionary
        And Indexing: The sorting gets performed
        And Indexing: The sorted list of legitimate words is stored in a text file

   Scenario: Indexed Sorting of Legitimate Words and results stored in CSV file
      Given Indexing: A directory with a list of documents and a list of legitimate words are to be sorted
       When Indexing: On searching and indexing
       Then Indexing: The strings which have high probability of being words are found through a numeric weight calculated for each word, comparing it to a matrix of weights in a dictionary
        And Indexing: Sorting is performed
        And Indexing: The sorted list of legitimate words is stored in a CSV file


   Scenario: Indexed Sorting of Legitimate Words and results stored in PDF file
      Given Indexing: A directory containing list of documents
        And Indexing: A list of legitimate words are to be sorted
       When Indexing: The searching and indexing is performed
       Then Indexing: The strings comprising high probability of being words are found through a numeric weight calculated for each word, comparing it to a matrix of weights in a dictionary
        And Indexing: The sorting is performed
        And Indexing: The sorted list of legitimate words is stored in a PDF file

   Scenario: Removal of duplicates and the results stored in a Text file
      Given Indexing: A list of documents placed in a directory
        And Indexing: The duplicate words have to be removed from the contents of the document
       When Indexing: The searching and indexing is done
       Then Indexing: The strings containing high probability of being words are found through a numeric weight calculated for each word, comparing it to a matrix of weights in a dictionary and sorting is performed
        And Indexing: Only the unique entries from the sorted list of all the legitimate words is stored in a text file
        And Indexing: The remaining duplicates are removed

   Scenario: Removal of duplicates and the results stored in a CSV file
      Given Indexing: A directory with a list of documents
        And Indexing: The duplicate words should be removed from the contents of the document
       When Indexing: The Indexing takes place
       Then Indexing: The strings holding high probability of being words are found through a numeric weight calculated for each word, comparing it to a matrix of weights in a dictionary and sorting is performed
        And Indexing: Only the unique entries from the sorted list of all the legitimate words is stored in a CSV file
        And Indexing: The duplicates are removed

   Scenario: Removal of duplicates and the results stored in a PDF file
      Given Indexing: A directory stored with a list of documents
        And Indexing: The duplicate words are to be removed from the contents of the document
       When Indexing: The search followed by indexing is performed
       Then Indexing: The strings which have high probability of being words are found through a numeric weight calculated for each word, comparing it to a matrix of weights in a dictionary and sorting is performed
        And Indexing: Only the unique entries from the sorted list of all the legitimate words is stored in a PDF file
        And Indexing: The rest of the duplicates are removed



