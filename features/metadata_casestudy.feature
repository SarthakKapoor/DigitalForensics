Feature: Forensics Meta Data Case Studies

  Background: The following scenarios are from real life case studies of Forensics Image Meta Data Search based on actual steps and procedures taken to find the digital evidence and investigate the crime

    Scenario: Camera type, make, model and GPS coordinates meta data search
       Given An investigation of 2014 case into the child abduction and trafficking
         And There are several identities and geographical locations of the potential suspects
         And The case is included with international and domestic elements and collaboration with law enforcement agencies associated
         And Consultation of the Internet Crimes Against Children task force takes place contributing to their resources to solve the case
         And A Municipality organisation from Minnesota, united states is then tasked with the local investigation for the area
         And A 35-year-old male is arrested as a possible offender
         And The city requests the assistance of Minnesota County Sheriff’s Office Intelligence Unit
         And The digital forensics experts to complete a comprehensive examination of the devices seized during the arrest to provide legitimate evidence
        When The meta data search for camera type, make and models are started for images files acquired from the PC and mobile devices
         And Amongst the devices presented for digital forensic examination is a mobile phone with a 12 mega pixel camera capable of capturing high definition images used for taking pictures of the missing children
        Then It is found that many of the images captured by and extracted from the phone contained a pixel resolution of 1080 x 900
         And The metadata directly linked the photos to the device they were first found
         And During the digital forensics examination, the experts collect evidence to support charges for the possession, manufacture, and transmission of child abduction and trafficking as the pictures included adult body parts, specifically hands and images of infants
         And Some of the images were also having the GPS Coordinates which confirmed the area codes from where the children went missing
         And The information presented with the case file indicated that the suspect was a local commercial and residential painter
         And Using a monitor with 42” high definition,  a paint-like substance on the hands in the photos is observed, which correlated to the suspect’s trade as a painter by County Sheriff’s Office detectives, in April 2015, state of Minnesota
         And A high level of detail was witnessed within the fingerprints after removing the pornographic details, investigators submitted five images to the Minnesota County Sheriff’s Office Fingerprint unit with a request for analysis, as detectives further enlarged the pictures on the monitor
         And The argument of “someone else did it” is a common defense in the field of digital forensics because it often is not possible to determine who actually used a device Therefore, the potential of a fingerprint association becomes vitally important




