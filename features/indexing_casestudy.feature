Feature: Forensics Indexing Case Studies

   Background: The following scenarios are from real life case studies of Forensics Indexing based on actual steps and procedures taken to find the digital evidence and investigate the crime

     Scenario: Disk Image Reformation Case Study 1
        Given Employee (A) suddenly resigns from the organisation (B) and starts a new organisation in direct competition
          And The Digital Forensics personal or organisation is briefed about the event by the legal department of the organisation
          And The PC previously used by employee (A) is sent for examination as an evidential copy of all data on the hard disk drive is made and preserved
         When The disk image is created through indexing for doing a detailed analysis of the data to find any evidence
         Then The deleted files are recovered and a clear disk image containing all data in all parts is analysed
          And The email files and other official files which had trace of them being sent to a private email account that could be potential evidence is sent to the organisation(B)
          And A full report is included in a suitable format containing the disk image for the organisation to take a legal action towards employee(A)

      Scenario: Disk Image and Memory Snapshot Generation Case Study 2
         Given A partner in an information firm left their job and The partner started their own business
           And The Partner is accused of acquiring proprietary documents and files on a laptop provided by the firm
           And The Partner deletes all the evidence and overwrites some of it to cover any tracks leading back to them, claiming that a prolific virus had destroyed documents
           And The Partner produces the laptop with the claim that even though there are missing documents, none of them are relevant
          When The disk drive of the partner is generated for finding proprietary information through search and indexing process
           And The deleted files were backed up through a memory snapshot rollback mechanism
          Then The complete disk image was retrieved along with the overwritten and deleted proprietary files
           And A detailed report containing all the information was generated in a suitable file format
           And The evidence of illegal client lists and the list themselves were also discovered



