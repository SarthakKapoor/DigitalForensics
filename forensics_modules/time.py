import time

# To display/print time in gmt format
print(time.gmtime(0))

time.gmtime(0xffffffff)

print(time.time())

# To display/print the Epoch unit of time
Epoch = time.time()

print(Epoch)

# To display/print the Epoch unit of time in gmt format
print(time.gmtime(int(Epoch)))

now = time.time()

# To display/print the time of the local time zone
print(time.localtime(int(now)))

print(time.timezone)
