import hashlib


# Function to generate the hash digest of sha256 for the input string
def sha_hash_string(hash_string):
    Sha_hash = hashlib.sha256(hash_string.encode()).hexdigest()
    return Sha_hash


hash_string = 'Digital Forensics'
output_hash = sha_hash_string(hash_string)

print(output_hash)
