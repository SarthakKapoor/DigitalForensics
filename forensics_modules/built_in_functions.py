a = 27

print(hex(a))

print(bin(a))

# The hex() and bin() inbuilt functions provides forensics investigators with
# the capability to display the data variables as different bases

PrintList = range(20)

# The range() inbuilt function provides forensics investigators with
# the option to store and display data in the form of lists.
# The other data structures such as Tuples and Dictionaries are also very useful for the same.

print(list(PrintList))

PrintList = range(4, 22)

print(list(PrintList))

PrintList = range(4, 22, 3)

print(list(PrintList))
