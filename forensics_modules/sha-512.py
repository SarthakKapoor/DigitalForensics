import hashlib


# Function to generate the hash digest of sha512 for the input string
def sha_hash_string(hash_string):
    sha_hash = hashlib.sha512(hash_string.encode()).hexdigest()
    return sha_hash


hash_string = 'Digital Forensics'
output_hash = sha_hash_string(hash_string)

print(output_hash)
