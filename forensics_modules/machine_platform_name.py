import sys
import platform

# Inbuilt function to display/print the System information
print("System Platform:" + sys.platform)
print("Machine:" + platform.machine())
