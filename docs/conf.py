import os
import sys

sys.path.insert(0, os.path.abspath('..'))

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
    'sphinx.ext.mathjax',
    'sphinx.ext.ifconfig',
]

templates_path = []

source_suffix = ['.rst', '.md']

master_doc = 'index'

project = u'The Digital Forensics Project'
copyright = u'2018, Sarthak Kapoor'
author = u'Sarthak Kapoor'

version = u'0'

release = u'1'

language = None

exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

pygments_style = 'sphinx'

todo_include_todos = False

html_theme = 'sphinx_rtd_theme'

htmlhelp_basename = 'SphinxExampleProjectdoc'

latex_elements = {

}

latex_documents = [
    (master_doc, 'Digital Forensics Project.tex', u'Digital Forensics Project Documentation',
     u'Digital Forensics', 'manual'),
]

man_pages = [
    (master_doc, 'digitalforensicsproject', u'Digital Forensics Documentation',
     [author], 1)
]

texinfo_documents = [
    (master_doc, 'Digital Forensics Project', u'Digital Forensics Documentation',
     author, 'Digital Forensics Project', 'Digital Forensics Repository',
     'Miscellaneous'),
]

intersphinx_mapping = {'https://docs.python.org/': None}
