.. Sphinx Example Project documentation master file, created by
   sphinx-quickstart on Tue Oct 25 09:18:20 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Digital Forensics Project documentation
==================================================

Contents:

.. toctree::
   :maxdepth: 4

   modules.rst

Index and tables
==================

* :ref:`genindex`
* :ref:`search`

