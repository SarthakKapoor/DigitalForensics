import ntplib
import time

# Network Time Protocol
NIST = 'nist1-macon.macon.ga.us'  # NTP certified server
ntp = ntplib.NTPClient()  # NTP client object
ntp_response = ntp.request(NIST)  # Trigger for time

if ntp_response:   # To verify if the response is received
    now = time.time()
    diff = now - ntp_response.tx_time
    # To get the time elapsed between the start of a period in time to the current time
    print('The difference is:'),
    print(diff),
    print('Number of seconds:'),
    print('The network delay is:'),
    print(ntp_response.delay),
    print('UTC: NIST :' + time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime(int(ntp_response.tx_time)))),
    print('UTC: SYSTEM :' + time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime(int(now))))
else:
    print('There is no response from the Time Service')
