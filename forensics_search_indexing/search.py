import argparse
import os
import logging
import sys

log = logging.getLogger('main.search_main')

# Sizes of word in bytes format
MIN_WORD = 5
MAX_WORD = 15
MAX_PHRASE = 80

PREDECESSOR_SIZE = 80
WINDOW_SIZE = 240

SPACE = ' '


# To parse command line arguments
def commandline():
    parser = argparse.ArgumentParser('Forensic Search and Indexing')

    parser.add_argument('-v', '--verbose', help="enables printing of additional program messages",
                        action='store_true')
    parser.add_argument('-k', '--keyWords', type=validate_read_operation, required=True,
                        help="specify the file containing search words")
    parser.add_argument('-p', '--keyPhrases', type=validate_read_operation, required=True,
                        help="specify the file containing search phrases")
    parser.add_argument('-t', '--srchTarget', type=validate_read_operation, required=True,
                        help="specify the target file to search")
    parser.add_argument('-m', '--theMatrix', type=validate_read_operation, required=True,
                        help="specify the weighted matrix file")

    global gl_args  # Global object for parsing arguments

    gl_args = parser.parse_args()

    message_display("Command line processed: Successfully")

    return


# Test case:
# Check if the read operation works for the provided path
def validate_read_operation(the_file):
    if not os.path.exists(the_file):
        raise argparse.ArgumentTypeError('File does not exist')

    if os.access(the_file, os.R_OK):
        return the_file
    else:
        raise argparse.ArgumentTypeError('File is not readable')


# To print message if the parsed command line argument is verbose
def message_display(msg):
    if gl_args.verbose:
        print(msg)

    return


# Function to search for the keywords present in the files or documents in the given directory
def word_search():
    word_search = set()

    phrase_search = set()

    try:
        file_words = open(gl_args.keyWords)
        for line in file_words:
            word_search.add(line.strip())
    except:
        log.error('Keyword File Failure: ' + gl_args.keyWords)
        sys.exit()
    finally:
        file_words.close()

    try:
        file_phrase = open(gl_args.keyPhrases)
        for line in file_phrase:
            phrase_search.add(line.strip())
    except:
        log.error('Phrase File Failure: ' + gl_args.keyPhrases)
        sys.exit()
    finally:
        file_phrase.close()

    log.info('Search Words')
    log.info('Input File: ' + gl_args.keyWords)
    log.info(word_search)

    log.info('Search Phrases')
    log.info('Input File: ' + gl_args.keyPhrases)
    log.info(phrase_search)

    try:
        target_file = open(gl_args.srchTarget, 'rb')
        ba_target = bytearray(target_file.read())
    except:
        log.error('Target File Failure: ' + gl_args.srchTarget)
        sys.exit()
    finally:
        target_file.close()

    target_size = len(ba_target)

    # Enable logging

    log.info('Target of Search: ' + gl_args.srchTarget)
    log.info('File Size: ' + str(target_size))

    ba_copy_target = ba_target

    word_check = Matrix()

    for i in range(0, target_size):
        character = chr(ba_target[i])
        if not character.isalpha() and character != SPACE:
            ba_target[i] = 0

    index_of_words = []

    cnt = 0
    for i in range(0, target_size):
        character = chr(ba_target[i])

        if character.isalpha() or character == SPACE:
            cnt += 1
        else:

            if MIN_WORD <= cnt <= MAX_PHRASE:

                new_phrase = ""
                for z in range(i - cnt, i):
                    new_phrase = new_phrase + chr(ba_target[z])

                new_phrase = new_phrase.lower()

                for eachPhrase in phrase_search:

                    if eachPhrase in new_phrase:
                        buffer_print(new_phrase, i - cnt, ba_copy_target, i - PREDECESSOR_SIZE, WINDOW_SIZE)
                        cnt = 0
                        print()

                split_word_list = new_phrase.split()

                for each_word in split_word_list:

                    if each_word in word_search:
                        buffer_print(each_word, i - cnt, ba_copy_target, i - PREDECESSOR_SIZE, WINDOW_SIZE)
                        index_of_words.append([each_word, i - cnt])
                        cnt = 0
                        print()
                    else:
                        if word_check.probable_word(each_word):
                            index_of_words.append([each_word, i - cnt])
                        cnt = 0
            else:
                cnt = 0

        print_found_words(index_of_words)

    return


# Function to print the file headers
def heading():
    print("Offset  00  01  02  03  04  05  06  07  08  09  0A  0B  0C  0D  0E  0F  ASCII")
    print("------------------------------------------------------------------------------------------------")

    return


# Function to find the exact offset of the keywords words and printing them in a byte array
def buffer_print(word, direct_offset, buff, offset, hex_size):
    print("Found: " + word + " At Address: "),
    print("%08x     " % direct_offset)

    heading()

    for i in range(offset, offset + hex_size, 16):
        for j in range(0, 17):
            if j == 0:
                print("%08x     " % i),
            else:
                byte_value = buff[i + j]
                print("%02x " % byte_value)
                print("      ")

        for j in range(0, 16):
            byte_value = buff[i + j]
            if 0x20 <= byte_value <= 0x7f:
                print("%c" % byte_value)
            else:
                print('.')

        print()

    return


# Function to print the found keywords
def print_found_words(word_list):
    print("Index of All Words")
    print("---------------------")

    word_list.sort()

    for entry in word_list:
        print(entry)

    print("---------------------")

    print()

    return


# Class to generate the weights of the words and store in a matrix for comparison
class Matrix:
    weightedMatrix = set()

    def __init__(self):
        try:
            matrix_file = open(gl_args.theMatrix, 'rb')
            for line in matrix_file:
                value = line.strip()
                if len(value) > 2:
                    self.weightedMatrix.add(int(value, 16))
        except:
            log.error('Matrix File Error: ' + gl_args.theMatrix)
            message_display("Matrix File Load Error")
            sys.exit()
        finally:
            matrix_file.close()

        return

    # Function to check the existence of the word in the matrix of words by comparing the generated weights
    def probable_word(self, the_word):

        if len(the_word) < MIN_WORD:
            return False
        else:
            base = 96
            word_weight = 0

            for i in range(4, 0, -1):
                char_value = (ord(the_word[i]) - base)
                shift_value = (i - 1) * 8
                char_weight = char_value << shift_value
                word_weight = (word_weight | char_weight)

            if word_weight in self.weightedMatrix:
                return True
            else:
                return False
