import logging
import time

from forensics_search_indexing import search

if __name__ == '__main__':   # Main function call to run the program
    SEARCH_VERSION = '1.0'

    # To enable logging
    logging.basicConfig(filename='search.log', level=logging.DEBUG, format='%(asctime)s %(message)s')

    # To parse arguments
    search.commandline()

    log = logging.getLogger('main.Search')
    log.info("search initiated")

    start_time = time.time()  # Function call to log the starting time

    search.word_search()  # Function call to trigger keyword search

    end_time = time.time()  # Function call to log the ending time

    duration = end_time - start_time

    logging.info('Time Elapsed: ' + str(duration) + ' seconds')
    # To get the total time taken by the search function to search for the keyword
    logging.info('')
    logging.info('Program Ran Successfully')
