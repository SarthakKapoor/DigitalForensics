import os
import argparse
import time
import csv
import hashlib
import logging

log = logging.getLogger('main.file_system_hash')


def commandline():
    parser = argparse.ArgumentParser('Python file system hashing... FileSystemHash')

    parser.add_argument('-v',
                        '--verbose',
                        help='allows progress messages to be displayed',
                        action='store_true')

    group = parser.add_mutually_exclusive_group(required=True)

    # Group for the selection should be mutually exclusive and required

    group.add_argument('--md5',
                       help='specifies MD5 algorithm',
                       action='store_true')
    group.add_argument('--sha256',
                       help='specifies SHA256 algorithm',
                       action='store_true')
    group.add_argument('--sha512',
                       help='specifies SHA512 algorithm',
                       action='store_true')

    parser.add_argument('-d',
                        '--rootPath',
                        type=validate_directory,
                        required=True,
                        help='specify the root path for hashing')
    parser.add_argument('-r',
                        '--reportPath',
                        type=validate_directory_writable,
                        required=True,
                        help='specify the path for reports and logs will be written')

    global gl_args  # Global objects for parsing arguments
    global gl_hash_type

    gl_args = parser.parse_args()

    if gl_args.md5:
        gl_hash_type = 'MD5'
    elif gl_args.sha256:
        gl_hash_type = 'SHA256'
    elif gl_args.sha512:
        gl_hash_type = 'SHA512'
    else:
        gl_hash_type = 'Unknown'
        logging.error('Unknown Hash Type Specified')

    message_display('Command line processed: Successfully')
    return


def traverse():
    process_count = 0
    error_count = 0

    open_csv = CSVWriter(os.path.join(gl_args.reportPath, 'fileSystemReport3.csv'),
                         gl_hash_type)

    log.info('Root Path: ' + gl_args.rootPath)

    # For loop for traversing through all the files present in the root directory
    for root, dirs, files in os.walk(gl_args.rootPath):

        # For loop for fetching the file names and generating a hash value for each file
        for file in files:
            f_name = os.path.join(root, file)
            result = hash_file(f_name, file, open_csv)

            # Condition for incrementing the process count if the hash value is generated successfully for every file
            if result is True:
                process_count += 1

            # Condition for incrementing the error count if the hash value is not generated successfully for every file
            else:
                error_count += 1

    open_csv.close_writer()
    return process_count


# Function for generating the hash values for every file or document
def hash_file(the_file, simple_name, o_result):
    # Test condition for checking if the path provided by user is correct
    if os.path.exists(the_file):
        # Test condition for checking if the path provided by user is not a link
        if not os.path.islink(the_file):
            # Test condition for checking if the path provided by user is not a valid file format
            if os.path.isfile(the_file):
                try:

                    f = open(the_file, 'rb')
                except IOError:

                    log.warning('Open Failed: ' + the_file)
                    return
                else:
                    try:

                        rd = f.read()
                    except IOError:

                        f.close()
                        log.warning('Read Failed: ' + the_file)
                        return
                    else:
                        the_file_stats = os.stat(the_file)
                        (mode, ino, dev, n_link, uid, gid, size,
                         a_time, m_time, c_time) = os.stat(the_file)

                        message_display('Processing File: ' + the_file)

                        file_size = str(size).encode()

                        modified_time = time.ctime(m_time).encode()
                        access_time = time.ctime(a_time).encode()
                        created_time = time.ctime(c_time).encode()

                        owner_id = str(uid).encode()
                        group_id = str(gid).encode()
                        file_mode = bin(mode).encode()

                        # Generation of hash values as per user selection
                        if gl_args.md5:
                            hash_value = hashlib.md5(rd).hexdigest()
                        elif gl_args.sha256:
                            hash_value = hashlib.sha256(rd).hexdigest()
                        elif gl_args.sha512:
                            hash_value = hashlib.sha512(rd).hexdigest()
                        else:
                            log.error('Hash not Selected')

                            print('=' * 20)
                            f.close()

                        o_result.csv_row_writer(simple_name, the_file, file_size,
                                                modified_time, access_time, created_time,
                                                hash_value, owner_id, group_id, file_mode)
                        return True
            else:
                log.warning('[{} Skipped NOT a File]'.format(repr(simple_name)))
                return False
        else:
            log.warning('[{} Skipped NOT a File]'.format(repr(simple_name)))
            return False
    else:
        log.warning('[{} Path does NOT exist]'.format(repr(simple_name)))
        return False


# Test case:
# Check if the path of the directory provided by the user is correct
def validate_directory(the_dir):
    if not os.path.isdir(the_dir):
        raise argparse.ArgumentTypeError('Directory does not exist')

    if os.access(the_dir, os.R_OK):
        return the_dir
    else:
        raise argparse.ArgumentTypeError('Directory is not readable')


# Test case:
# Check if the write operation works for the provided path
def validate_directory_writable(the_dir):
    if not os.path.isdir(the_dir):
        raise argparse.ArgumentTypeError('Directory does not exist')

    if os.access(the_dir, os.W_OK):
        return the_dir
    else:
        raise argparse.ArgumentTypeError('Directory is not writable')


def message_display(msg):
    if gl_args.verbose:
        print(msg)
    return


class CSVWriter(object):
    def __init__(self, file_name, hash_type):
        try:

            self.csvFile = open(file_name, "w", newline='')
            self.writer = csv.writer(self.csvFile, delimiter=',', quoting=csv.QUOTE_ALL)
            self.writer.writerow(('File', 'Path', 'Size', 'Modified Time', 'Access Time',
                                  'Created Time', hash_type, 'Owner', 'Group', 'Mode'))
        except:
            log.error('CSV File Error')

    def csv_row_writer(self, file_name, file_path, file_size, m_time, a_time,
                       c_time, hash_val, own, grp, mod):
        self.writer.writerow((file_name, file_path, file_size, m_time,
                              a_time, c_time, hash_val, own, grp, mod))

    def close_writer(self):
        self.csvFile.close()
