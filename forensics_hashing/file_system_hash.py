import logging
import sys
import time

from forensics_hashing import file_system_hash_functions

logging.basicConfig(filename='file_system_hashing_log.log', level=logging.DEBUG, format='%(asctime)s%(message)s')
# To record the time and date of each time the program is run

file_system_hash_functions.commandline()  # Function call to fetch command line arguments

start_time = time.time()  # Function call to log the starting time

logging.info('Name of System:' + sys.platform)  # To log the system name and version name
logging.info('Name of Version:' + sys.version)

files_processed = file_system_hash_functions.traverse()  # Function call to traverse and hash the directory files

end_time = time.time()  # Store time and find the time duration
duration = end_time - start_time

logging.info('Processed Files:' + str(files_processed))
# To keep a track of which files have been covered and which are still left to be covered

logging.info('Time Elapsed(in Seconds):' + str(duration) + 'seconds')
# To get the total time taken by the hash function to hash all the files in the directory

logging.info('Program Ran Successfully')

file_system_hash_functions.message_display("Program End")

if __name__ == '__main__':   # Main function call to run the program
    file_system_hash_functions_VERSION = '1.0'
